#!/usr/bin/python
"""Calculate constraining likelihoods given some data"""

import hasty_observables as obs
import hasty_data as dat
import shift_params as SP
import numpy as np
import scipy.optimize as opt
from hasty_settings import *

C = 3e5 # Speed of light in km/s/Mpc


def sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov):
	"""Get the SNe chi-squared for a given offset a"""
	# dm_milne = 5log10(C*[z + 0.5z^2]) - 5 log10(H0)
	# NOTE: Uncomment the following line on Ubuntu 11.10, but not on Mac OS X.
	# Presumably they fixed a bug somewhere along the way and it causes this var to be transposed.
	# ddm_model = np.transpose(ddm_model)
	v = ddm_data - ddm_model - ddm_milne - a
	mat = np.dot(icov, np.transpose(v))
	x2 = np.dot(v, mat)
	return float(x2)

def sn_best_fit_offset(ddm_data, ddm_model, ddm_milne, icov):
	"""Find the H0 which minimizes the chi-squared"""
	ddm_data = np.matrix(ddm_data)
	ddm_model = np.transpose( np.matrix(ddm_model) )
	ddm_milne = np.matrix(ddm_milne)
	# Find deltaDM offset which minimises the chi-squared (initial guess: 1.0)
	try:
		offset = opt.leastsq(sn_chisq, 1.0, args=(ddm_data, ddm_model, ddm_milne, icov))
		a = float(offset[0]) # Extract offset from array
		x2 = float(sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov)) # Chi-squared for best-fit value
	except:
		raise
		# Fail gracefully, by rejecting this run
		print "ERROR: Failed to find minimum chi-squared for supernova data (hasty_constraints.py)"
		a = 1e5
		x2 = BIGL
	return x2, a

def supernovae_union2(idd, sne_data):
	"""Get the chi-squared for UNION2 SNe data, from chain with id=idd"""
	
	# Load params calculated from model
	try:
		ddm = obs.deltaDM(idd)
	except:
		print "\tsupernovae() failed - couldn't load model data"
		return BIGL
	
	# Construct array needed to calculate the offset
	# dm_model is the deltaDM for the calculated LTB model
	# ['dm_milne'] is a precalculated reference Milne model (with a fiducial H0)
	# sne['dm'] is the precalculated vector of supernova dm from UNION2
	dm_model = map(lambda zz: ddm.DM(zz), sne_data['z']) # Model deltaDM for each SN z value
	
	# Find the distance modulus offset which minimises the chi-squared
	# Use the result to return the chi-squared
	# (Corrected reference DM = ddM_data - ddm_milne - a)
	# icov is the inverted covariance matrix
	l_sne, a = sn_best_fit_offset(sne_data['dm'], dm_model, sne_data['dm_milne'], sne_data['icov'])
	
	return l_sne, a
	

def supernovae_wright(idd):
	"""Get the chi-squared for SNe data, from chain with id=idd"""
	
	sne = dat.sne_data_wright() # Load observational SNe data
	# Load params calculated from model
	try:
		ddm = obs.deltaDM(idd)
	except:
		print "\tsupernovae() failed - couldn't load model data"
		return BIGL
	
	# Find the chi-squared for the SNe data
	l_sne = 0.0
	n = 0
	for sn in sne:
		n += 1
		l_sne += (sn['dm'] - ddm.DM(sn['z']))**2.0 / (sn['err']**2.0)
	#l_sne /= float(n) # Don't use the reduced chi-squared, it's not valid here!
	return float(l_sne)
	

def local_hubble_rate(idd):
	"""Find the chi-squared for the H0 data"""
	
	hubble = dat.hubble_data() # Load H0 measurement data
	# Load params calculated from model
	try:
		op = obs.obsParams(idd)
	except:
		print "\tlocal_hubble_rate() failed - couldn't load model data"
		return BIGL
	
	hub = hubble[0] # Only one datapoint
	l_hub = (hub['H0'] - op.H0*100.)**2.0 / (hub['errH0']**2.0)
	return l_hub
	

def cmb_shift(idd):
	"""Find the chi-squared for the Shift parameter data"""
	
	shdat = dat.shift_params() # Load Shift parameter *data*
	# Load params calculated from model
	try:
		sh = obs.obsParams(idd)
	except:
		raise
		print "\tshift() failed - couldn't load model data"
		return BIGL
	
	
	# Calculate shift params from the calc. model data (H's are in 100h units)
	S = 100.0 * sh.Hdec * sh.dAdec / (2.*C * ( (1.+sh.zdec)**0.5 - 1.) )
	H0eds = 100.0 * (1. + sh.zdec)**(-1.5) * sh.Hdec
	
	cov = shdat['cov']
	#np.linalg.inv(cov)
	
	# Calculate chi-squared (log likelihood) using shift param covariance matrix
	v = [ S - shdat['S'], H0eds - shdat['H0eds'] ] # Vector of deviations
	l_shift = np.dot(np.transpose(v), np.dot(np.linalg.inv(cov), v))
	
	return l_shift


def ksz_bang_time_proximity(idd, maxgrad=1e-10):
	"""Boolean likelihood - decide if kSZ observer at z=1 has their LSS too close to the non-asymptotically-homogeneous Bang time region"""
	# maxgrad is the (absolute) maximum tolerated gradient, in Myr/Mpc
	
	# Load params calculated from model
	try:
		sh = obs.obsParams(idd)
	except:
		raise
		print "\tksz_bang_time_proximity() failed - couldn't load model data"
		return BIGL, 0.0
	
	# Check if redshifts were OK
	# lz is not supposed to be a likelihood!
	lz = sh.zdec - sh.zlss
	
	# Return a big likelihood if it's too close, otherwise return zero
	# (Acts like a prior on being too close to the inhomogeneity)
	if abs(sh.ksz_lss_flatness) > maxgrad:
		print "*** WARNING: Too close to kSZ LSS, r_lss(ksz_zmax) =", sh.r_asym_lss, " and dtb/dr =", sh.ksz_lss_flatness
		return BIGL, lz
	else:
		return 0.0, lz

def local_bang_time_proximity(idd, maxgrad=1e-10):
	"""Boolean likelihood - decide if local observer at z=0 has their LSS too close to the non-asymptotically-homogeneous Bang time region"""
	# maxgrad is the (absolute) maximum tolerated gradient, in Myr/Mpc
	# Load params calculated from model
	try:
		sh = obs.obsParams(idd)
	except:
		raise
		print "\tlocal_bang_time_proximity() failed - couldn't load model data"
		return BIGL, 0.0
	
	# Return a big likelihood if it's too close, otherwise return zero
	# (Acts like a prior on being too close to the inhomogeneity)
	if abs(sh.local_lss_flatness) > maxgrad:
		print "*** WARNING: Too close to local LSS, r_lss(z=0) =", sh.rlss, " and dtb/dr =", sh.local_lss_flatness
		return BIGL
	else:
		return 0.0


def ksz(idd):
	"""Get the chi-squared between the model and the kSZ data"""
	
	obs_data = dat.ksz_data() # Get observational data
	mod_data = obs.kszParams(idd) # Get model data
	mod_z = np.array(mod_data.z)
	
	x2 = 0.0
	# Go through each observed data point and find the chi-sq with the model
	for i in range(len(obs_data['vp'])):
		# Get nearest datapoint in z from model data
		idx = (np.abs(mod_z - obs_data['z'][i])).argmin()
		
		if np.abs(mod_z[idx] - obs_data['z'][i]) < 0.005:
			# It's close enough
			x2 += (obs_data['vp'][i] - mod_data.vp[idx])**2. / obs_data['err'][i]**2.0
		else:
			# There was no model data for this point (model probably failed)
			# So insert speed of light (will produce very high chi-sq)
			x2 += (obs_data['vp'][i] - 3e5)**2. / obs_data['err'][i]**2.0
	return x2

# Old code for shift params
#S = 100.0 * sh.Hdec * sh.dAdec / (2.*C * ( (1.+sh.zdec)**0.5 - 1.) )
#dH = sh.H0 * (1. + sh.zdec)**1.5 / sh.Hdec
#l_S = (S - shdat['S'])**2.0 / (shdat['err_S']**2.0)
#l_dH = (dH - shdat['dH'])**2.0 / (shdat['err_dH']**2.0)
