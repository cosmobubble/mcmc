#!/usr/bin/python
"""Graph for publication.
	"""

import pylab as P
import numpy as np

def get_ksz_obs_data():
	"""Get observed kSZ velocities, the ones quoted in GBH"""
	d = np.genfromtxt("../bubble/obsdata/GBH_kSZ_data.dat", unpack=True)
	z = d[1]
	vp = d[2]*1e-3 # Units are now (10^3 km/s)
	err = d[3]*1e-3
	return z, vp, err

# Get kSZ vp(z) data
o_z, o_vp, o_err = get_ksz_obs_data()

# Redshifts of kSZ samples
z = [0.18, 0.2, 0.22, 0.23, 0.25, 0.29, 0.45, 0.55, 0.56]

# Beta(z) for Atb = 0
# Bubble parameters: [0.75, 0.75, 2000.0, 0.0, 1800.0, 0.5]
# ksz-chi-sq=192.800454729, sne-chi-sq=545.520806394
vp_0 = [7837.56, 8220.52, 8451.01, 8503.81, 8471.55, 7821.33, 2376.04, 892.146, 808.983]

# Beta(z) for Atb != 0
# Bubble parameters: [0.75, 0.75, 2000.0, -1200.0, 1800.0, 0.5]
# ksz-chi-sq=89.6731887535, sne-chi-sq=569.497297815
vp_1 = [5115.29, 5446.87, 5766.62, 5943.76, 6395.23, 7644.62, 3875.81, 3132.99, 3147.0]

# Convert units of beta(z)
vp_0 = map(lambda x: x*1e-3, vp_0)
vp_1 = map(lambda x: x*1e-3, vp_1)


# Plot vp(z) curves
P.subplot(111)

# beta(z) curves
P.plot(z, vp_0, 'k-', label="$A_B = 0$")
P.plot(z, vp_1, 'r-', label="$A_B = 1200$")

# kSZ data
P.errorbar(o_z, o_vp, yerr=o_err, fmt=',', color='k') # Error bars from real kSZ data

# Axis labels
P.ylabel(r"$\beta$ $(z)$ $[10^3 kms^{-1}]$", fontdict={'fontsize':'x-large', 'weight':'heavy'})
P.xlabel("$z$", fontdict={'fontsize':'x-large', 'weight':'heavy'})

P.legend()

P.show()
