#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import matplotlib.mlab as mlab
import sys

idd = int(sys.argv[1])
A = Ana.Analyse(idd)

cols = ['r-', 'g-', 'b-', 'y-', 'k-', 'c-']
x = []
for varname in A.data_varnames:
	tmp = A.spectrum(varname)
	tmpx = tmp[1:]/tmp[0]
	x.append(tmpx)

P.subplot(221)
print len(x)
for i in range(len(x)):
	P.plot(x[i], cols[i])
P.xlim([0.0, 100.0])
P.yscale('log')

# Build list of types
x = []
y = []
types = ['a', 'b', 'r']
cols = ['g.', 'r.', 'k,']
for t in types:
	x.append([])
	y.append([])

# Get bad steps
for s in A.steps:
	for i in range(len(types)):
		if s['rej']==types[i]:
			x[i].append(s['h'])
			y[i].append(s['Ok'])
	
P.subplot(222)
for i in range(len(types)):
	P.plot(x[i], y[i], cols[i])
P.xlabel("h")
P.ylabel("Ok")


# Display spectrogram
P.subplot(223)
Pft, f, t = mlab.specgram(x[2], NFFT=256) # f freq, t time
P.contourf(t, f, Pft)
P.show()
