#!/usr/bin/python
"""Run the MCMC"""
# {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5}

import hasty_mcmc
import hasty_utils as U
import sys
import numpy.random as nran

idd = sys.argv[1] # Get ID for this chain

# Define constraints to use
constr = {"H0":True, "Shift":True, "KLSS":False, "LLSS":True, "Sne":True }

# Define priors and starting point, guess initial covariance
priors = [ [0.0,1.0], [0.0, 1.0], [0.0,6e3], [-8e3,8e3], [0.0,10e3], [0.1,1.1] ]

#-------------------------------------------------------------------------------
# Initial proposal density
#p = U.starting_point_from_priors(priors)

# The Bang time is the important factor here, so let's artificially set it to ~0
#nran.seed()
#p[3] = nran.uniform(-1e2, 1e2)
#p[4] = nran.uniform(-1e2, 1e2)

# Can fix a parameter by settings all its covariance matrix elements to zero

#-------------------------------------------------------------------------------
# Learned proposal density
#p = [0.434, 0.381, 3600.0, 264.4, 165.8, 0.55]
#cov = U.diag( [0.01**2., 0.01**2., 50.0**2.,  50.0**2.,  50.0**2.,  0.005**2. ] )

"""
# Learned proposal densities
# From 5600, 5601
p_notb = [0.77, 0.85, 2900., 0.0, 1e3, 0.50]
cov_notb = [ [3e-4, 2e-4, 0.4, 0.0, 0.0, 1e-4],
		[2e-4, 2e-4, 0.29, 0.0, 0.0, 0.0],
		[0.4, 0.29, 1.3e4, 0.0, 0.0, 0.1],
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[1e-4, 0.0, 0.1, 0.0, 0.0, 2e-4] ]

p_notb2 = [0.81, 0.9, 3000., 0.0, 1e3, 0.45]
cov_notb2 = [ [3e-5, 2e-5, 0.04, 0.0, 0.0, 1e-5],
		[2e-5, 2e-5, 0.029, 0.0, 0.0, 0.0],
		[0.04, 0.029, 1.3e3, 0.0, 0.0, 0.01],
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[1e-5, 0.0, 0.01, 0.0, 0.0, 2e-5] ]

# From 5500, 5501, 5502
p_tb_lowh = [0.68, 0.79, 2850., 500., 800., 0.4]
cov_tb_lowh = [ [7e-4, 6e-4, 1.3, -2., 0., 0.],
				[6e-4, 5e-4, 0.8, -1.9, 0., 0.],
				[1.3, 0.8, 8e3, 5e2, 0., 0.2],
				[-2., -1.9, 5e2, 3e4, 0., 1.],
				[0., 0., 0., 0., 2e4, 0.],
				[0., 0., 0.2, 1., 0., 1e-4] ]

# Guessed! For high-H bang time
p_tb_bigh = [0.68, 0.79, 2850., 2500., 300., 0.8]

# Set the actual proposal/start point

# NO BANG TIME
#p = p_notb2
#cov = cov_notb2

# BANG TIME
#p = p_tb_lowh
#cov = cov_tb_lowh

# GUESSED BANG TIME WITH VIABLE H0
#p = p_tb_lowh
#cov = cov_tb_lowh

# FIXED H0 WITH BANG TIME
cov_fixedh = [ [7e-4, 6e-4, 1.3, -2., 0., 0.],
				[6e-4, 5e-4, 0.8, -1.9, 0., 0.],
				[1.3, 0.8, 8e3, 5e2, 0., 0.],
				[-2., -1.9, 5e2, 3e4, 0., 0.],
				[0., 0., 0., 0., 2e4, 0.],
				[0., 0., 0., 0., 0., 0.] ]

#p = [0.7, 0.8, 2500.0, 3000.0, 4000.0, 0.7]
#cov = cov_fixedh
#for aa in range(len(cov)):
#	for bb in range(len(cov[aa])):
#		cov[aa][bb] *= 0.1

#T = nran.uniform(2.0, 10.0) # Assign temperature randomly


cov_510 = [ [0.0015, 0.0012, 3.0921, -4.0841, 2.803, 0.0],
            [0.0012, 0.001, 2.7399, 1.862, -0.9684, 0.0],
            [3.0921, 2.7399, 30302.98, 111667.2558, -63456.6528, 0.0],
            [-4.0841, 1.862, 111667.2558, 1648802.318, -1023686.5266, -0.0],
            [2.803, -0.9684, -63456.6528, -1023686.5266, 770045.3602, 0.0],
            [0.0, 0.0, 0.0, -0.0, 0.0, 0.0001] ]

cov_30june = U.load_cov_matrix("cov1005-30jun")


# 30 June MCMC run with Atb!=0, after initial burn-in run
p = [0.857, 0.857, 1740.0, -1440., 7705.0, 0.74]
#cov = U.diag([0.001**2., 0.001**2., 10.0**2., 10.0**2., 15.0**2., 0.001**2.])
cov = cov_30june
for aa in range(len(cov)):
	for bb in range(len(cov[aa])):
		cov[aa][bb] *= 0.1
"""

########################
### NO H0 CONSTRAINT ###
"""
#p = U.starting_point_from_priors(priors)
p = [0.794, 0.868, 3021., -152., 6219., 0.58]
p[0] += nran.uniform(-0.002, 0.002)
#p[1] = p[0] # Match these initially because they probably want to be the same
p[2] += nran.uniform(-2e2, +2e2)
p[3] += nran.uniform(-2e2, +2e2)
p[4] += nran.uniform(-5e2, +5e2)
p[5] += nran.uniform(-0.05, 0.05)

#cov = U.diag( [0.003**2., 0.003**2., 20.0**2.,  20.0**2.,  20.0**2.,  0.003**2. ] )
#Used for 1270 (4):
cov = [ [0.0006, 0.0002, -0.2485, -3.861, 1.2757, 0.0002],
        [0.0002, 0.0003, 2.1456, -0.9725, -0.2548, 0.0],
        [-0.2485, 2.1456, 101850., 71700., -47780., -3.1354],
        [-3.861, -0.9725, 71700., 105911., -54751., -2.7368],
        [1.2757, -0.2548, -47780., -54751., 73733., 1.7172],	
        [0.0002, 0.0, -3.1354, -2.7368, 1.7172, 0.0003] ]

for aa in range(len(cov)):
	for bb in range(len(cov[aa])):
		cov[aa][bb] *= 0.1
"""

"""
p = [0.641, 0.767, 2899.393, 2221.964, 6986.244, 0.393]

cov = [ [0.0116, 0.0077, 23.9293, -140.1692, 35.4829, 0.0022],
        [0.0077, 0.0052, 16.465, -92.4815, 25.6657, 0.0014],
        [23.9293, 16.465, 91106.214, -246302.9748, 84628.2881, 4.0064],
        [-140.1692, -92.4815, -246302.9748, 1824595.6367, -391069.2692, -27.1911],
        [35.4829, 25.6657, 84628.2881, -391069.2692, 363175.3059, 2.2655],
        [0.0022, 0.0014, 4.0064, -27.1911, 2.2655, 0.0007] ]

for aa in range(len(cov)):
	for bb in range(len(cov[aa])):
		cov[aa][bb] *= 0.1
"""

# Redone 1100 run, this time with the proper Union2 supernova analysis
p = [0.837, 0.835, 1725., -1285., 7750., 0.739]
p[2] += nran.uniform(-50., 50.)
p[3] += nran.uniform(-50., 50.)
p[4] += nran.uniform(-50., 50.)

cov = [ [0.0004, 0.0004, 1.1435, -4.569, -2.814, 0.0001], 
        [0.0004, 0.0004, 1.4088, -4.1265, -1.2548, -0.0], 
        [1.1435, 1.4088, 12482.3783, -6381.4563, 20008.5322, -1.4018], 
        [-4.569, -4.1265, -6381.4563, 105889.0653, 127929.9745, -1.419],
        [-2.814, -1.2548, 20008.5322, 127929.9745, 261077.5012, -6.8802],
        [0.0001, -0.0, -1.4018, -1.419, -6.8802, 0.0006] ]

for aa in range(len(cov)):
	for bb in range(len(cov[aa])):
		cov[aa][bb] *= 0.2

T = 1.0

########################

# Once burned in, use informed covariance matrix and starting point
#cov = U.load_cov_matrix("proposal.cov")
#-------------------------------------------------------------------------------

# Run the MCMC
mc = hasty_mcmc.HastyMCMC(p, priors, cov, idd=idd, num_runs=10000, constraint_mode=constr, temperature=T)
mc.mcmc_control()

# Save calculated matrices
avg, newcov = mc.calculate_cov()
U.save_cov_matrix(avg, "means-"+str(idd));
U.save_cov_matrix(newcov, "covariance-"+str(idd));
