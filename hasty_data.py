#!/usr/bin/python
"""Load observed data from files"""

import numpy as np
import numpy.linalg as linalg

def sne_data():
	"""Load SNe data points from a file"""
	#return sne_data_wright()
	return sne_data_union2()


def sne_data_union2():
	"""Load SNe data points and covariance matrix from SCP UNION2 compilation"""
	
	covfile = "../bubble/obsdata/SCPUnion2_covmat_sys.txt"
	datafile = "../bubble/obsdata/SCPUnion2_mu_vs_z.txt"
	C = 3e5 # Speed of light, km/s
	
	# Load covariance matrix (with systematics) and find its inverse
	cov = np.matrix( np.genfromtxt(covfile, unpack=True) )
	icov = linalg.inv(cov)
	
	# Load vector of SNe data points
	# Columns: (0) Sne name, (1) Redshift, (2) Distance modulus, (3) DM error
	# See http://supernova.lbl.gov/Union/descriptions.html#Magvsz
	sn_data = np.genfromtxt(datafile, unpack=True)
	
	# Get reference Milne model
	# (Uses H0=100 km/s/Mpc, but this doesn't really matter)
	ddm_milne = map(lambda zz: 5.*np.log10(1e5*(C/100.)*(zz + 0.5*zz**2.)), sn_data[1])
	
	# Construct data array
	sn = { 	'z': sn_data[1], 'dm': sn_data[2], 'err': sn_data[3], 'icov': icov, 
			'dm_milne': ddm_milne }
	
	return sn


def sne_data_wright():
	"""Load SNe data points from Ned Wright's binned compilation"""
	data = []
	
	# Open SNe data file and get values
	f = open("../bubble/obsdata/SNe_Wright.dat", 'r')
	for line in f.readlines():
		line = line.split(" ")
		if len(line) > 5 and line[0]!="#":
			zz = float(line[2])
			ddm = float(line[3])
			errdm = float(line[4])
			sn = {'z': zz, 'dm': ddm, 'err': errdm}
			data.append(sn)
	
	return data


def shift_params():
	"""Get data for shift params (derived from WMAP data using wmap_constraints.py)"""
	f = open("../bubble/obsdata/WMAP_derived_shift_params.dat")
	for line in f.readlines():
		line = line.split("\t")
		if len(line) >= 2:
			S = float(line[0])
			H0eds = float(line[1])
			cov11 = float(line[2])
			cov12 = float(line[3])
			cov22 = float(line[4])
	f.close()
	cov = [[cov11, cov12], [cov12, cov22]]
	return { 'S':S, 'H0eds':H0eds, 'cov':cov }


def hubble_data():
	"""Load Hubble H0 data"""
	data = []
	
	# Open H0 data file and get values
	f = open("../bubble/obsdata/Riess_H0.dat", 'r')
	for line in f.readlines():
		line = line.split("\t")
		if len(line) >= 2:
			H0 = float(line[0])
			errH0 = float(line[1])
			hub = { 'H0': H0, 'errH0': errH0 }
			data.append(hub)
	return data

def ksz_data():
	"""Load kSZ velocities, the ones quoted in GBH"""
	dat = np.genfromtxt("../bubble/obsdata/GBH_kSZ_data.dat", unpack=True)
	d = {'z':dat[1], 'vp':dat[2], 'err':dat[3]}
	return d
