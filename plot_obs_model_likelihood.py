#!/usr/bin/python
"""Show the distribution of likelihoods as calculated during the MCMC"""

import hasty_accepted_analysis
import pylab as P
import numpy as np

A = hasty_accepted_analysis.AnalyseObs()

l = []
lsne = []
lhub = []
for chain in A.chain:
	for model in chain:
		l.append( model['l'] )
		lsne.append( model['lsne'] )
		lhub.append( model['lhub'] )


# Make kSZ histogram
P.subplot(111)
P.hist(lhub, bins=100, facecolor='b', range=(0., 20), label="H0")
P.hist(lsne, bins=100, facecolor='r', range=(0., 20), label="SNe")
P.hist(l, bins=100, facecolor='g', range=(0., 20.), label="Total")
P.xlabel("Likelihood")
P.legend()

P.show()
