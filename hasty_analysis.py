#!/usr/bin/python
"""Analyse a stored MCMC chain"""

import random
import sys
import numpy.fft

try:
	idd = int(sys.argv[1])
	CHAIN_FILE = "mcmc_record-" + str(idd)
except:
	CHAIN_FILE = ""


def output_cov_matrix(cov, filename):
	"""Output a machine-readable covariance matrix"""
	f = open(filename, 'w')
	for row in cov:
		for col in row:
			f.write( str(round(col, 4)) + "\t" )
		f.write("\n")
	f.close()

def read_cov_matrix(filename):
	"""Read a machine-readable covariance matrix and build an array from it"""
	f = open(filename, 'r')
	cov = []
	for line in f.readlines():
		tmp = line.split("\t")[:-1]
		if len(tmp) > 2:
			# Make sure it's all floats
			for i in range(len(tmp)):
				tmp[i] = float(tmp[i])
			cov.append(tmp)
	return cov





class Analyse(object):
	
	def __init__(self, idd=-1):
		"""Initialise the analyser, load the chain into memory"""
		self.chain = [] # Only good steps (the ones that were accepted)
		self.steps = [] # All of the steps, including bad ones
		self.data_varnames = ["Otot", "Ok", "Wk", "Atb", "Wtb", "h"]
		
		# Check whether to look at an individual file, or to use the argument
		if idd==-1:
			fname = CHAIN_FILE
		else:
			fname = "mcmc_record-" + str(idd)
		
		# Load the data
		f = open(fname, 'r')
		for line in f.readlines():
			tmp = line.split("\t")
			if len(tmp) >= 11:
				Otot = float(tmp[0])
				Ok = float(tmp[1])
				Wk = float(tmp[2])
				Atb = float(tmp[3])
				Wtb = float(tmp[4])
				h = float(tmp[5])
				ltot = float(tmp[6])
				lsne = float(tmp[7])
				lH0 = float(tmp[8])
				lshift = float(tmp[9])
				lkszprox = float(tmp[10])
				llocalprox = float(tmp[11])
				rej = str(tmp[12])
				
				# Check if there was additional data
				if len(tmp) > 15:
					sn_offset = float(tmp[14])
					ddA = float(tmp[14])
					dH0 = float(tmp[15])
					dHlss = float(tmp[16])
					S = dHlss * ddA  / (2.*3e5 * (1.+1090.79) * ( (1.+1090.79)**0.5 - 1.) )
					H0eds = dHlss*(1.+1090.79)**(-1.5)
				else:
					ddA = 0.0
					dH0 = 0.0
					dHlss = 0.0
					S = 0
					H0eds = 0
				
				row = {	"Otot": Otot, "Ok":Ok, "Wk": Wk, "Atb": Atb, "Wtb": Wtb,
						"h": h, "l": ltot, "lsne": lsne, "lH0": lH0, 
						"lshift": lshift, "lkszprox": lkszprox, 
						"llocalprox": llocalprox, "rej": rej, "data_H0": dH0,
						"data_Hlss": dHlss, "data_dA": ddA, "S":S, "H0eds":H0eds }
				
				self.steps.append(row)
		
		# Split-off the accepted steps
		for step in self.steps:
			if step['rej']=="a":
				self.chain.append(step)
	
	def column(self, colname, cut=0.0, lcut=1e10):
		"""Return a list which is a column of data from the chain. Reject the first [cut] fraction of all chains"""
		col = []
		# Get first chain member after the cut
		idx = int( cut*len(self.chain) )
		
		for row in self.chain[idx:]:
			# Check likelihood cut
			if row['l'] <= lcut:
				col.append( row[colname] )
		return col
	
	def mean(self, varname, lcut=1e10):
		"""Find the mean value of a variable"""
		avg = 0.0
		n = 0
		for row in self.chain:
			if row['l'] <= lcut:
				n += 1
				avg += row[varname]
		avg = avg / float(n)
		return avg
	
	def variance(self, varname):
		"""Find the variance of a variable"""
		u = self.mean(varname) # Get the mean
		
		var = 0.0
		n = 0
		for row in self.chain:
			n += 1
			var += (row[varname] - u)**2.0
		var = var / float(n)
		return var
	
	def ACF(self, varname, shuffle=False, lag=40):
		"""Find the autocorrelation function of a variable"""
		var = self.variance(varname)
		x = self.column(varname) # List of the variables
		N = len(x)-1
		acf = []
		
		# Diagnostic; shuffles list to destroy correlation
		if shuffle:
			random.shuffle(x)
			
		# Find the ACF for all of the discrete lags
		for l in range(N):
			i = 0
			ac = 0.0
			while i < (N - l):
				ac += x[i]*x[i-l] # Unnormalised
				i += 1
			if var > 0.0:
				acf.append(ac / (var * float(N-l)) )
			
		return acf[:40]
	
	
	def covariance(self, lcut=1e10):
		"""Calculate the covariance matrix (so far) for the MCMC run"""
		# First, calculate the averages
		varnames = self.data_varnames
		dimensions = len(varnames)
		n = len(self.column('l', lcut=lcut)) # No. of samples passing the likelihood cut
		
		# Build empty covariance matrix
		cov = []
		for i in range(dimensions):
			tmp = []
			for j in range(dimensions):
				tmp.append(0.0)
			cov.append(tmp)
		
		# Get the ordered array of values, and get averages
		vals = []
		avg = []
		for varname in varnames:
			vals.append( self.column(varname, lcut=lcut) )
			avg.append( self.mean(varname, lcut=lcut) )
		
		# Now, calculate the variances
		for i in range(dimensions):
			for j in range(dimensions):
				# Go through each datapoint
				for k in range(n):
					cov[i][j] += (avg[i] - vals[i][k]) \
							  *  (avg[j] - vals[j][k])
				cov[i][j] /= float(n)
		return avg, cov
	
	def spectrum(self, varname):
		"""Find the discrete Fourier transform power spectrum of a variable"""
		x = self.column(varname)
		cft = numpy.fft.rfft(x) # Complex Fourier transform
		return cft.conj()*cft
		
	def pretty_print_cov(self, avg, cov):
		"""Pretty-print the covariance matrix"""
		
		print "\nMean:"
		for item in self.data_varnames:
			print "<" + item + ">\t", 
		print ""
		
		for item in avg:
			print str(round(item, 3)) + "\t",
		print "\n"
		
		# Output variable names
		for varname in self.data_varnames:
			print varname,
		print ""
		
		# Output covariance matrix
		print "Covariance:"
		for row in cov:
			for col in row:
				print str(round(col, 4)) + "\t", 
			print ""
		print ""
	
	def print_acceptance_rate(self):
		"""Print the number of accepted steps"""
		a = 0
		b = 0
		r = 0
		p = 0
		k = 0
		for row in self.steps:
			rej = row['rej']
			if rej == "a":
				a += 1
			elif rej == "b":
				b += 1
			elif rej == "k":
				k += 1
			elif rej == "p":
				p += 1
			elif rej == "r":
				r += 1
		print "Accepted:", round(100.*a/(a+b+r+p+k), 1), "%",
		print "Rejected:", round(100.*r/(a+b+r+p+k), 1), "%",
		print "Prox.kSZ.LSS:", round(100.*k/(a+b+r+p+k), 1), "%",
		print "Prox.loc.LSS:", round(100.*p/(a+b+r+p+k), 1), "%",
		print "Failed:", round(100.*b/(a+b+r+p+k), 1), "%",
		print "Total:", a+b+r+p+k, "Acc:", a

