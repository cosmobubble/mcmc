#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import numpy as np
import scipy.stats as stats
import math, sys
#from hasty_settings import *
#import matplotlib.ticker as ticker

# Set the max. no. of ticks that there can be on each axis
#Mticks = ticker.MaxNLocator(nbins=14, steps=[1, 3, 5, 7, 10])

NBINS = 20
NLEVELS = 8
stot = 0

root = int(sys.argv[1])
nchains = int(sys.argv[2])
burnin = float(sys.argv[3])

def get_centroids(vals):
	"""Get bin centroids"""
	cent = []
	for i in range(len(vals)-1):
		cent.append(0.5*(vals[i]+vals[i+1]))
	return cent

def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	#slevels.append(-1e-15) # Very small number at the bottom
	print "****", stats.scoreatpercentile(linz, 95), stats.scoreatpercentile(linz, 5)
	return slevels[::-1]


A = []
for i in range(nchains):
	A.append( Ana.Analyse(root+i) )

#A = A[1:2]

# Make contour plots
vn = A[0].data_varnames
vn_label = ["$\Omega_k$", "$\Omega_{k_1}$", "$\lambda_k$ $[Gpc]$", "$A_{t_B}$ $[Gyr]$", "$\lambda_{t_B}$ $[Gpc]$", "$h$"]
rescale = [1.0, 1.0, 1e-3, 1e-3, 1e-3, 1.0]
#vn = ["Otot", "Ok", "Wk", "h"]
#vn += ["S", "H0eds"]

# Try to set the figure size, in inches
P.figure(figsize=(20., 13.))

for i in range(len(vn)):
	for j in range(len(vn)):
		if(j>i):
			# Collect data from all chains
			xx = []
			yy = []
			for a in A:
				xx += a.column(vn[i], lcut=burnin)
				yy += a.column(vn[j], lcut=burnin)

			xx = map(lambda x_x: x_x*rescale[i], xx)
			yy = map(lambda y_y: y_y*rescale[j], yy)
			
			print j, len(xx), len(yy)
			# Plot these data
			P.subplot(len(vn)-1, len(vn)-1, 1+(i)+(len(vn)-1)*(j-1))
			z, x, y = np.histogram2d(xx, yy, bins=NBINS)
			# Don't forget to take transpose of z!
			P.contourf(get_centroids(x), get_centroids(y), z.T, get_sig_levels(z), cmap=P.cm.PuBu_r) ###NLEVELS)
			P.contour(get_centroids(x), get_centroids(y), z.T, get_sig_levels(z), colors="black")
			
			# Set font size
			fontsize = 16.
			ax = P.gca()
			kk = 0
			for tick in ax.xaxis.get_major_ticks():
				tick.label1.set_fontsize(fontsize)
				kk += 1
				# Remove every other label if they're likely to overlap
				if vn[i] in ["Wk", "Wtb"] and kk%2==0:
					tick.label1.set_visible(False)
				if vn[i] in ["Otot", "Ok"] and kk%2==1:
					tick.label1.set_visible(False)
			kk = 0
			for tick in ax.yaxis.get_major_ticks():
				tick.label1.set_fontsize(fontsize)
				kk += 1
				if vn[j] in ["Wk", "Wtb"] and kk%2==0:
					tick.label1.set_visible(False)
				if vn[j] in ["Otot", "Ok"] and kk%2==1:
					tick.label1.set_visible(False)
			
			# Limit the number of ticks on the axis
			#ax.xaxis.set_major_locator(Mticks)
			#ax.yaxis.set_major_locator(Mticks)
			
			
			# Put labels in appropriate places
			if(i==0):
				P.ylabel(vn_label[j], weight="bold", size="24")
			if(j==len(vn)-1):
				P.xlabel(vn_label[i], weight="bold", size="24")

#caption = "  \n  $\lambda_k\colon$ $Gpc$  \n  $A_B\colon$ $Gyr$  \n  $\lambda_B\colon$ $Gpc$  \n  $H_0\colon$ $kms^{-1}Mpc^{-1}$  \n"
#P.figtext(0.65, 0.65, caption, size="xx-large", bbox={'ec':'black', 'fc':'white', 'ls':'solid', 'lw':1.0})

# Get means for the variables
print ""
for var in vn:
	print "<" + str(var) + "> =",
	for a in A:
		print a.mean(var),
	print ""
print ""
#P.show()

P.figure(1).subplots_adjust(wspace=0.3, hspace=0.3)
P.savefig('pub-mcmc-triangle.png')
