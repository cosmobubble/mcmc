#!/usr/bin/python

import pymc
import model

S = pymc.MCMC(model, db='pickle')
S.sample(iter=10000, burn=5000, thin=2)
help(S)
#pymc.Matplot.plot(S)
