"""Variables for use in all of the MCMC/analysis code"""

# Number of chains that were made in the MCMC run (numbered from 100)
NUM_CHAINS = 2

# The ID number of the first chain
CHAIN_ROOT = 6000

# How long to wait before killing a process in seconds (ten minutes)
TIMEOUT = 10*60.

# Maximum no. of bad runs to tolerate before failing
MAXBAD = 1e4

# Large likelihood value, should be automatically rejected
BIGL = 1e15

# Paths to working directory of driven MCMC code
WORKING_DIR = "/home/phil/postgrad/kSZ2011/bubble/"
EXEC_PATH = "/home/phil/postgrad/kSZ2011/bubble/bubble"
#WORKING_DIR = "/Users/bullp/postgrad/bubble/"
#EXEC_PATH = "/Users/bullp/postgrad/bubble/bubble"

# Default set of constraints to use
CONSTR_DEFAULT = {"H0":True, "Shift":True, "KLSS":True, "LLSS":True, "Sne":True, "kSZ":False }

# Maximum bang time gradient to allow at some last-scattering radius before rejecting
# (Approx.) maximum dt_b = 10^-3 Myr (~1% of t_recomb) for dr ~ 150 Mpc
# Anything with an (absolute) gradient above this is rejected
TBMAXGRAD = 1e-7

# Number of decimal places to round the parameter values to, before they are 
# passed to the Bubble code. This was originally set to 4, but for parameters 
# which are of order 1e-1, this had a large effect on the results.
NUM_DP = 6
