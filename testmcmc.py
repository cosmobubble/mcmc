#!/usr/bin/python

import math
import lcdm_parameters as M
import numpy.random as nran
import pylab as P
import time

t_start = time.time()

# MCMC parameters
CHAIN_LENGTH = 10000 # Length of the MCMC chain
INFORMED = False

################################################################################
# Generate data points of H(z) which are consistent with the model
# H0 = 72, Om = 0.3, OL = 0.7
nran.seed()
LCDM = M.Model(72, 0.3)

oz = []
for i in range(100):
	oz.append( nran.uniform() ) # Redshifts between 0 and 1
oH = [] # "Observed" H(z)
sH = [] # "Observed" error on H(z) values

for z in oz:
	hz = LCDM.H(z)*0.3
	h = nran.normal(hz, hz*0.01) # 1% error on H(z) measurements
	sh = nran.normal(hz*0.01, hz*0.001) # Simulate "quoted" error, not exactly 1%
	oH.append(h)
	sH.append(sh)

print "\tGenerated H(z) sample for reference model"


################################################################################
def new_trial_vector(p, s, rmin, rmax):
	"""Given the current parameter vector, get a new trial parameter vector"""
	pnew = []
	for i in range(len(p)):
		tmp = rmin[i]
		# Don't let the parameter go outside the desired range
		while tmp <= rmin[i] or tmp >= rmax[i]:
			tmp = nran.normal(p[i], s[i])
		pnew.append( tmp )
	return pnew

def new_trial_vector_informed(p, s, rmin, rmax):
	"""Given the current parameter vector, get a new trial parameter vector (TUNED)"""
	pnew = rmin
	# Don't let the parameter go outside the desired range
	while pnew[0] <= rmin[0] or pnew[0] >= rmax[0] or pnew[1] <= rmin[1] or pnew[1] >= rmax[1]:
		pnew = nran.multivariate_normal(p, s)
	return pnew

def model_likelihood(mod):
	"""Test a model against the dataset and return the (unnormalised) likelihood"""
	l = 0.0
	for i in range(len(oz)):
		l += ( mod.H(oz[i])*mod.Om - oH[i] )**2.0 / sH[i]**2.0
	return l / float( len(oz) )

def model_accepted(lnew, lold):
	"""Decide whether a model should be accepted"""
	
	# Select uniformly-distributed random variable
	alpha = nran.uniform()
	
	# Test if the likelihood ratio is above the threshold
	if math.exp(0.5 * (lold - lnew)) > alpha:
		return True
	else:
		return False


################################################################################
p_init = [70, 0.25] # Initial guess for parameter set [H0, Om]
s_init = [ 2, 0.01] # Initial variances for selecting trial params

# Initial covariance matrix
if INFORMED:
	# Found empirically from prev. MCMC run
	s = []
	s.append([62.0336493915, -0.203295960931])
	s.append([-0.203295960931, 0.000682690797331])
	p = [72.3443785067, 0.30161113129]
else:
	s = s_init
	p = p_init

rmin = [20, 0.0] # Minimum value of parameter to consider
rmax = [110, 1.0] # Maximum value of parameter to consider
l = model_likelihood( M.Model(p[0], p[1]) )

record = [] # Record of what happened in the chain
n = 0 # No. of points in chain

print "\tRunning chain (" + str(CHAIN_LENGTH) + " hops)"
while n < CHAIN_LENGTH:
	
	# Trial a new model
	if INFORMED:
		pnew = new_trial_vector_informed(p, s, rmin, rmax)
	else:
		pnew = new_trial_vector(p, s, rmin, rmax)
	mnew = M.Model(pnew[0], pnew[1])
	lnew = model_likelihood(mnew)
	
	# Decide whether to accept the new model
	A = model_accepted(lnew, l)
	
	if(A):
		# Update everything if the model was accepted
		n += 1
		p = pnew
		l = lnew
		rec = { "L": l, "Acc": A, "Params": p } # Likelihood, accepted, parameters
		record.append(rec)
	else:
		# Record that the model was rejected and then try again
		rec = { "L": lnew, "Acc": A, "Params": pnew }
		record.append(rec)

################################################################################
# Extract only data from chains that were accepted
H0 = []
Om = []
L = []
for rec in record:
	if rec['Acc']:
		H0.append(rec['Params'][0])
		Om.append(rec['Params'][1])
		L.append(rec['L'])

################################################################################
# Calculate the covariance matrix
print "\tCalculating covariance matrix"
C11 = 0.0 # H0, H0
C12 = 0.0 # H0, Om
C22 = 0.0 # Om, Om
CC = []

# Calculate means
uH0 = 0.0
uOm = 0.0
N = 0
for i in range(len(H0)):
	uH0 += H0[i]
	uOm += Om[i]
	N += 1
uH0 = uH0 / float(N)
uOm = uOm / float(N)
u = [uH0, uOm]

print "----------------"
print "<H0> =", uH0
print "<Om> =", uOm
print ""

N = 0
for i in range(len(H0)):
	C11 += (H0[i] - uH0)*(H0[i] - uH0)
	C12 += (H0[i] - uH0)*(Om[i] - uOm)
	C22 += (Om[i] - uOm)*(Om[i] - uOm)
	N += 1
C11 = C11 / float(N)
C12 = C12 / float(N)
C22 = C22 / float(N)

# Populate Cov matrix
CC.append([C11, C12])
CC.append([C12, C22])

print "Var(H0) =", C11
print "Cov(H0.Om) =", C12
print "Var(Om) =", C22
print ""
print "S(H0) =", math.sqrt(uH0*uH0 - C11)
print "S(Om) =", math.sqrt(uOm*uOm - C22)
print "----------------"

################################################################################
# Generate multivariate Gaussian distribution informed by initial cov matrix
preX = []
preY = []
iCC = []
iCC.append([s_init[0]**2.0, 0.0])
iCC.append([0.0, s_init[1]**2.0])
for i in range(int(CHAIN_LENGTH*0.1)):
	x, y = nran.multivariate_normal(p_init, iCC)
	preX.append(x)
	preY.append(y)

################################################################################
# Generate multivariate Gaussian distribution informed by MCMC cov matrix measurement
postX = []
postY = []
for i in range(int(CHAIN_LENGTH*0.1)):
	x, y = nran.multivariate_normal(u, CC)
	postX.append(x)
	postY.append(y)

################################################################################
# Plot likelihood contours in (H0,Om) space
print "\tPlotting likelihood contours and MC history"

print len(H0), "of", len(record), "trials were accepted (" + str(round(100.*len(H0)/len(record), 2)) + "%)"

# Plot a contour plot
P.subplot(221)
P.plot(H0, Om, label="Random walk")
P.plot(postX, postY, 'r,', label="Fitted distribution")
P.plot(preX, preY, 'y,', label="Prior distribution")
P.xlabel("H0")
P.ylabel("$\Omega_m$")
P.legend()

P.subplot(222)
P.plot(H0, L, "r.")
P.xlabel("H0")
P.ylabel("Likelihood")
P.yscale('log')

P.subplot(223)
P.plot(H0, 'r-')
P.ylabel("H0")

P.subplot(224)
P.plot(Om, 'g-')
P.ylabel("$\Omega_m$")

print "Chain took", round(time.time() - t_start, 2), "seconds."

P.show()
