#!/usr/bin/python
"""COnvenience functions for use in the MCMC"""

import numpy.random as nran

def load_cov_matrix(fname):
	"""Load a covariance matrix from a file"""
	cov  = []
	f = open(fname, 'r')
	for line in f.readlines():
		tmp = line.split(" ")
		if len(tmp) > 1:
			row = map(lambda x: float(x), tmp[:-1])
			cov.append(row)
	f.close()
	
	return cov

def save_cov_matrix(cov, fname):
	"""Save a covariance matrix in a standard format"""
	f = open(fname, 'w')
	for row in cov:
		for col in row:
			f.write(str(col) + " ")
		f.write("\n")
	f.close()

def diag(vals):
	"""Make a diagonal square matrix with these values"""
	m = []
	for i in range(len(vals)):
		m.append([])
		for j in range(len(vals)):
			if i==j:
				m[i].append(float(vals[i]))
			else:
				m[i].append(0.0)
	return m

def starting_point_from_priors(priors):
	"""Get a random starting point, selected from the priors"""
	p = []
	nran.seed()
	for bound in priors:
		# Get uniform random no. in prior range
		tmp = nran.uniform( bound[0], bound[1] )
		p.append(tmp)
	return p
