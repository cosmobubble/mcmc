#!/usr/bin/python
"""Run the MCMC"""
# {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5}
# {"offset_x":6, "offset_x2":7, "A2":8, "W2":9}

import hasty_mcmc_complex
import hasty_utils as U
import sys
import numpy.random as nran

idd = sys.argv[1] # Get ID for this chain

# Define constraints to use
constr = {"H0":True, "Shift":True, "KLSS":False, "LLSS":False, "Sne":True, "kSZ":True }

# Define priors and starting point, guess initial covariance
priors = [ 	[0.0,1.0], [0.0, 1.0], [0.0,6e3], [-10e3, 10e3], [1e2, 10e3], [0.25, 0.80],
			[-10e3, 10e3], [-4e3, 4e3], [-4.0, 4.0], [0.01, 10.0] ]

# Set the starting point, covariance matrix, and temperature
p = [0.858, 0.849, 2000.0, -5567.00093392, 6287.5718068, 0.504035650859, -4978.11483619, 3542.70674302, 0.0328069676014, 1.18736579765]

p = [0.7747, 0.8382, 1364.9684, -1719.7498, 932.7250, 0.4507, 957.3268, 
	 231.3429, 0.7746, 7.8992]

cov = U.diag( [0.001**2., 0.001**2., 0.0**2., 50.0**2., 50.0**2., 0.001**2., 30.0**2.,  20.0**2.,  0.001**2.,  0.05**2. ] )

T = 1.0

# Run the MCMC
mc = hasty_mcmc_complex.HastyComplexMCMC(p, priors, cov, idd=idd, num_runs=10000, constraint_mode=constr, temperature=T)
mc.mcmc_control()

# Save calculated matrices
avg, newcov = mc.calculate_cov()
U.save_cov_matrix(avg, "means-"+str(idd));
U.save_cov_matrix(newcov, "covariance-"+str(idd));
