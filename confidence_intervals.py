#!/usr/bin/python

import scipy.stats as stats
import numpy.random as nran

nran.seed()
data = nran.normal(0.0, 4.0, 10000)

print "Med.:", stats.scoreatpercentile(data, 50)
print "68.3%:", stats.scoreatpercentile(data, 15.85), stats.scoreatpercentile(data, 84.15)
print "90%:", stats.scoreatpercentile(data, 10), stats.scoreatpercentile(data, 90)
print "95%:", stats.scoreatpercentile(data, 5), stats.scoreatpercentile(data, 95)
