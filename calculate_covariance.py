#!/usr/bin/python
"""Trim chain of burn-in part and estimate covariance matrix"""

import hasty_analysis as Ana
import sys
import pylab as P
import numpy as np

try:
	idd = int(sys.argv[1])
	start = int(sys.argv[2])
except:
	print "ERROR: Expects 2 arguments: [id of chain] [starting point (cuts all samples before this)]"
	exit()

def mean(varname, chain):
	"""Find the mean value of a variable"""
	avg = 0.0
	n = 0
	for row in chain:
		n += 1
		avg += row[varname]
	avg = avg / float(n)
	return avg

def covariance(A, start):
	"""Calculate the covariance matrix (so far) for the MCMC run"""
	# First, calculate the averages
	varnames = A.data_varnames
	dimensions = len(varnames)
	chain = A.chain[start:]
	n = len(chain)
	print "Analysing", n, "accepted sample points."
	
	# Build empty covariance matrix
	cov = []
	for i in range(dimensions):
		tmp = []
		for j in range(dimensions):
			tmp.append(0.0)
		cov.append(tmp)
	
	# Get the ordered array of values, and get averages
	vals = []
	avg = []
	for varname in varnames:
		vals.append( A.column(varname)[start:] )
		avg.append( mean(varname, chain) )
	
	# Now, calculate the variances
	for i in range(dimensions):
		for j in range(dimensions):
			# Go through each datapoint
			for k in range(n):
				cov[i][j] += (avg[i] - vals[i][k]) \
						  *  (avg[j] - vals[j][k])
			cov[i][j] /= float(n)
	return avg, cov

A = Ana.Analyse(idd)

k = 0
s = []
a = []
ymin = []
ymax = []

for i in range(int(len(A.chain)/100)):
	start = int(len(A.chain)/100)*i
	avg, cov = covariance(A, start)
	s.append(start)
	a.append(avg[k])
	ymax.append( max( A.column(A.data_varnames[k])[start:] ) )
	ymin.append( min( A.column(A.data_varnames[k])[start:] ) )

P.subplot(111)
P.plot(s, a, 'r-')
P.plot(s, ymax, 'k-' )
P.plot(s, ymin, 'k-' )
P.show()
