#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import numpy as np
import math
from hasty_settings import *
import sys

ROOT = int(sys.argv[1])
NCHAINS = int(sys.argv[2])
BURNIN = float(sys.argv[3])

A = []
for i in range(NCHAINS):
	A.append( Ana.Analyse(ROOT+i) )

#vn = A[0].data_varnames
vn = ['Otot', 'Ok', 'Wk', 'h']
vnlabel = ['$\Omega_{tot}$', '$\Omega_{k}$', '$W_k$ (Gpc)', '$H_0$ (km/s/Mpc)']
cols = ['r', 'y', 'k', 'g', 'y', 'c', 'm']
bbins = []

# Plot distributions for each variable for combined chains
for i in range(len(vn)):
	pl = P.subplot(221+i)
	P.xlabel(vnlabel[i], fontsize='large')
	xx = []
	
	# Correct units
	if i==2:
		div = 1000.0
	elif i==3:
		div = 0.01
	else:
		div = 1.0
		
	for j in range(len(A)):
		xx += A[j].column(vn[i], lcut=BURNIN)
	xx = map(lambda a: a/div, xx)
	
	#n, bins, patches = P.hist(xx, 40, normed=True, ec='k')
	#P.plot(bins[1:], n, 'r-', ls='steps')
	y, x = np.histogram(xx, bins=30, normed=True)
	P.plot(x[1:], y, "k-", lw=1.0, label=vnlabel[i])
	pl.set_yticks([])
	if i==2:
		pl.set_xticks([3.0, 3.2, 3.4, 3.6])
	#P.legend(loc='upper left', markerscale=0.0, numpoints=1, prop={'size':'x-large'})
	#bbins.append(bins)
	#P.yscale('none')

"""
# Plot separate distributions for each variable for each chain
for j in range(len(A)):
	for i in range(len(vn)):	
		P.subplot(231+i)
		P.title(vn[i])
		
		#n, bins, patches = P.hist(A[j].column(vn[i]), 20, normed=1, facecolor=cols[i])
		y, x = np.histogram(A[j].column(vn[i], lcut=BURNIN), bins=bbins[i], normed=False)
		P.plot(x[1:], y, cols[j]+"-", drawstyle='steps', linewidth=1.1)
"""
P.show()
