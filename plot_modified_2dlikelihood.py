#!/usr/bin/python
"""Cobbled-together code to display 2d likelihood plots for MCMC chains with larger number of parameters."""

import numpy as np
import pylab as P


def get_sig_levels(z):
    """Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
    linz = z.flatten()
    linz = np.sort(linz)[::-1]
    tot = sum(linz)
    acc = 0.0; i=-1; j=0
    lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
    slevels = []
    for item in linz:
	acc += item/tot
	i+=1
	if(acc >= lvls[j]):
            j+=1
            slevels.append(item)
    slevels.append(-1e-15) # Very small number at the bottom
    return slevels[::-1]


# Columns containing model input parameters
cols = (0,1,2,3,4,5,6,7,8,9)
data = []; stat = []; like = []
for i in range(8):
    fname = "mcmc_record-2223"+str(i+1)
    data.append(np.loadtxt(fname, unpack=False, usecols=cols))

    # Column containing model run status (col 17)
    # dtype is the data type, and the argument is a prototype of the datatype you want to use
    stat.append( np.loadtxt(fname, dtype="a", usecols=(17,)) )
    like.append( np.loadtxt(fname, usecols=(10,11,12,13)) ) # Likelihoods, except kSZ 16, which seems to need special handling


# Only get rows that have been accepted
accepted = []; likeacc = [];
for i in range(len(stat)):
    for j in range(len(stat[i])):
        if stat[i][j]=='a':
            accepted.append(data[i][j])
            likeacc.append(like[i][j])
accdat = np.array(accepted).transpose()
like = np.array(likeacc).transpose()

# Burn-in: trim the first 20%
#idx = len(accdat[0])*0.2


# Print-out minimum likelihoods
xx = list(like[0])
lidx = xx.index(min(xx))
print "Min. total likelihood index:", lidx
print accdat.transpose()[lidx]
print like.transpose()[lidx]

print ">>>", len(data)


# Make contour plots
# {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5}
# {"offset_x":6, "offset_x2":7, "A2":8, "W2":9}
vn = ["Otot", "Ok", "Wk", "Atb", "Wtb", "h"]
vnidx = [0, 1, 2, 3, 4, 5]
for i in range(len(vn)):
    for j in range(len(vn)):
	if(j>i):
            P.subplot(len(vn)-1, len(vn)-1, 1+(i)+(len(vn)-1)*(j-1))
            z, x, y = np.histogram2d(accdat[vnidx[i]], accdat[vnidx[j]], bins=15)
            P.contourf(x[:-1], y[:-1], z.T, get_sig_levels(z))
            if(i==0):
                P.ylabel(vn[j], weight="bold", size="large")
            if(j==len(vn)-1):
                P.xlabel(vn[i], weight="bold", size="large")

P.show()
