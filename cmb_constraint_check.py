#!/usr/bin/python
"""Figure-out why the CMB constraints aren't working too well"""
import hasty_data
import numpy as np

# Best values (with lowest l) from MCMC
##S = 1.02283350099
##dH = 0.724215511775

# Best values from WMAP LCDM best fit
S = 0.888
dH = 0.515

# Void
##S = 1.02283350099
##dH = 0.724215511775

shdat = hasty_data.shift_params() # Load Shift parameter *data*
cov = shdat['cov']
np.linalg.inv(cov)

# Calculate chi-squared (log likelihood) using shift param covariance matrix
v = [ S - shdat['S'], dH - shdat['dH'] ] # Vector of deviations
print "(S, S0, dH, dH0) =", S, shdat['S'], dH, shdat['dH']
print "Deviations", v
print "Sigmas", np.sqrt(cov[0][0]), np.sqrt(cov[1][1]), np.sqrt(cov[0][1])

print ""
print "Basic (diagonal) chisq (S, dH, tot):", v[0]**2.0/cov[0][0], v[1]**2.0/cov[1][1], v[0]**2.0/cov[0][0] + v[1]**2.0/cov[1][1]

l_shift = np.dot(np.transpose(v), np.dot(np.linalg.inv(cov), v))
print "Full cov matrix chisq:", l_shift

print "\nCov matrix:", cov
print "Inverted cov matrix:", np.linalg.inv(cov)

cov[0][1] = 0.0
cov[1][0] = 0.0
l_shift2 = np.dot(np.transpose(v), np.dot(np.linalg.inv(cov), v))
print "Diag cov matrix chisq:", l_shift2
