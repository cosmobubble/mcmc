#!/usr/bin/python
"""Make a sample out of the samples with the highest likelihood"""

import hasty_analysis as Ana
from hasty_settings import *
import sys
import pylab as P
import numpy as np
import numpy.random as nran

CUT = 0.5e3 # Maximum -2 ln L to allow
NBINS = 10
NLEVELS = 8
stot = 0
cols = ['r-', 'b-', 'g-', 'y-', 'c-', 'm-']

root = int(sys.argv[1])
num = int(sys.argv[2])
CUT = float(sys.argv[3])

A = []
for k in range(num):
	idd = root + k
	A.append( Ana.Analyse(idd) ) # Get analysis object

# Get only the highest-likelihood part of the sample
def get_cut_sample(cut):
	"""Return a sample with some cut"""
	sample = []
	for k in range(len(A)):
		sample.append([])
		for i in range(len(A[k].steps)):
			ll = A[k].steps[i]['l']
			if ll < cut:
				sample[k].append(A[k].steps[i])
		#print "Chain", k, "gave", len(sample[k]), "samples out of", len(A[k].steps)
		#print "\tMin l:", min( A[k].column('l') ), " Max l:", max( A[k].column('l') )
	return sample

def get_column(idd, var, sample):
	"""Get a column from the sample"""
	dat = []
	for row in sample[idd]:
		dat.append(row[var])
	return dat

################################################################################

varnames = ['h', 'Otot', 'Ok', 'Wk']

lrange = [5000, 200, 100, 75, 50]
dg = 1.0/len(lrange)

for n in range(len(varnames)):
	P.subplot(221+n)
	
	# Plot summed histogram, for all chains
	greyness = 0.0
	ii = -1
	xbins=30
	for c in lrange:
		ii += 1
		
		# Set greyscale colour
		greyness += dg
		colspec = (greyness,greyness,greyness) # RGB tuple
		
		# Get sample from each chain  and add to grand sample
		sample = get_cut_sample(c)
		full_sample = []
		for j in range(len(A)):
			full_sample += get_column(j, varnames[n], sample)
		
		# Calculate histogram
		nn, bbins, patches = P.hist(full_sample, bins=xbins, label=str(c), fc=colspec, ec='k')
		# Check if we already have the bin layout for the histo; if so, use that to keep bins aligned
		if type(xbins) is int:
			xbins = bbins
	if n==0:
		P.legend(loc='upper left')
	P.xlabel(varnames[n])

# TESTING
print "-"*50

ss = get_cut_sample(50.0)[0]
ells = map(lambda x: x['l'], ss)
esses = map(lambda x: x['S'], ss)
idx = ells.index( min(ells) )
print "(S,dH,l)", ss[idx]['S'], ss[idx]['dH'], ss[idx]['l']
for key in ss[idx].keys():
	print "\t", key, ss[idx][key]


print "-"*50

"""
################################################################################
# Get low-l sample so we can get a covariance matrix out of it
cuts_to_make = [50, 75, 100, 200, 5000]
colours = ['r', 'g', 'b', 'y', 'c']
bbins = [30, 30, 30, 30, 30]

for jj in range(len(cuts_to_make)):
	# Get sample below some l threshold
	lowl_sample = get_cut_sample(cuts_to_make[jj])
	dictdata = []
	for chain in lowl_sample:
		dictdata += chain
	data = []
	for var in varnames:
		data.append( map(lambda x: x[var], dictdata) )

	# Calculate the means
	u = []
	for i in range(len(varnames)):
		uu = 0.0
		for val in data[i]:
			uu += val
		u.append(uu/len(data[i]))

	# Calculate the covariance matrix
	cov = []
	for i in range(len(varnames)):
		cov.append([])
		for j in range(len(varnames)):
			cc = 0.0
			for k in range(len(data[i])):
				cc += (data[i][k] - u[i]) * (data[j][k] - u[j])
			cov[i].append(cc/len(data[i]))

	# Create random sampled Gaussian with this covariance matrix
	nran.seed()
	this_sample = nran.multivariate_normal(u, cov, size=10000)

	# Plot the Gaussians
	for k in range(len(this_sample[0])):
		P.subplot(221+k)
		this_var = map(lambda x: x[k], this_sample)
		nn, bins2, something = P.hist(this_var, bins=bbins[k], linewidth=1.5, fc='none', ec=colours[jj], label=str(cuts_to_make[jj])+", N="+str(len(data[k])), normed=True)
		P.xlabel(str(varnames[k]))
		if k==0:
			P.legend(loc='upper left')
		if type(bbins[k]) is int:
			bbins[k] = bins2
"""

"""
################################################################################
# 
sample = get_cut_sample(CUT)
for n in range(len(varnames)):
	P.subplot(221+n)
	
	# Plot summed histogram, for all chains
	full_sample = []
	for j in range(len(A)):
		full_sample += get_column(j, varnames[n])
	nn, bins, patches = P.hist(full_sample, bins=20, label='All', ec='none', fc='#cdcdcd')
	
	# Plot histograms of likelihoods individually, for each chain
	for j in range(len(A)):
		try:
			hh, bb = np.histogram( get_column(j, varnames[n]), bins=bins )
			#P.plot(bb[:-1], hh, cols[j], drawstyle='steps', linewidth=1.5, label=str(j))
			P.hist(get_column(j, varnames[n]), bins=bins, ec=cols[j][0], fc='none', label=str(j))
		except:
			print "Failed for chain", j
			pass
	
	# Add labels to graph
	P.xlabel(varnames[n])
	if n==0:
		P.legend(loc='upper left')
		P.title("Likelihood threshold = " + str(CUT))
"""
###P.show()
