#!/usr/bin/python
"""Run the MCMC"""
# {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5}
# {"offset_x":6, "offset_x2":7, "A2":8, "W2":9}

import hasty_mcmc
import hasty_utils as U
import sys
import numpy.random as nran

idd = sys.argv[1] # Get ID for this chain

# Define constraints to use
#constr = {"H0":True, "Shift":True, "KLSS":False, "LLSS":True, "Sne":True, "kSZ":True }
constr = {"H0":True, "Shift":True, "KLSS":False, "LLSS":False, "Sne":True, "kSZ":True }

# Define priors and starting point, guess initial covariance
##priors = [ [0.0,1.0], [0.0, 1.0], [0.0,6e3], [-8e3,8e3], [0.0,10e3], [0.1,1.1] ]
priors = [ 	[0.0,1.0], [0.0, 1.0], [0.0,6e3], [-10e3, 10e3], [1e2, 10e3], [0.25, 0.80],
			[-10e3, 10e3], [-4e3, 4e3], [-4.0, 4.0], [0.01, 10.0] ]

#-------------------------------------------------------------------------------
# Initial proposal density
#p = U.starting_point_from_priors(priors)

# The Bang time is the important factor here, so let's artificially set it to ~0
#nran.seed()
#p[3] = nran.uniform(-1e2, 1e2)
#p[4] = nran.uniform(-1e2, 1e2)

# Can fix a parameter by settings all its covariance matrix elements to zero

#-------------------------------------------------------------------------------

# Set the actual proposal/start point
#T = nran.uniform(2.0, 10.0) # Assign temperature randomly

p = [0.858, 0.849, 2000.0, -5567.00093392, 6287.5718068, 0.504035650859, -4978.11483619, 3542.70674302, 0.0328069676014, 1.18736579765]

#p = [0.85, 0.85, 1500., -6500., 7000., 0.5, -4500.0, 3500.0, 0.036, 1.080]
cov = U.diag( [0.001**2., 0.001**2., 0.0**2., 50.0**2., 50.0**2., 0.001**2., 30.0**2.,  30.0**2.,  0.005**2.,  0.05**2. ] )
T = 2.0

# Once burned in, use informed covariance matrix and starting point
#cov = U.load_cov_matrix("proposal.cov")
#-------------------------------------------------------------------------------

# Run the MCMC
mc = hasty_mcmc.HastyMCMC(p, priors, cov, idd=idd, num_runs=10000, constraint_mode=constr, temperature=T)
mc.mcmc_control()

# Save calculated matrices
avg, newcov = mc.calculate_cov()
U.save_cov_matrix(avg, "means-"+str(idd));
U.save_cov_matrix(newcov, "covariance-"+str(idd));
