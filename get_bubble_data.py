#!/usr/bin/python
"""Get a full set of data from the files output by Bubble"""

import scipy.interpolate as interp
import hasty_observables as obs
C = 3e5

# Interpolate redshifts and r's
# Use gr, gz to get interpolator
def zinterp(rr, d):
	# Find the r array value that is closest to rr
	ii = 0
	minval = 1e5 # Set to large number initially
	for i in range(len(d['gr'])):
		diff = abs(d['gr'][i] - rr)
		if diff < minval:
			minval = diff
			ii = i
	# Find a slice of r array values near to the r value (5 values)
	if(ii <= 2):
		# Near the start of the array
		imin = 0
		imax = 4
	elif(ii >= len(d['gr']) - 3):
		# Near the end of the array
		imin = len(d['gr']) - 5
		imax = len(d['gr']) - 1
	else:
		# Somewhere in the middle
		imin = ii - 2
		imax = ii + 2
		
		# Build a spline using this slice of the arrays
		sp = interp.UnivariateSpline(d['gr'][imin:imax], d['gz'][imin:imax], k=3)
		return sp(rr)

def fetch(idd):
	idd = str(idd)
	# Lists of calculated variables
	r = [] # Comoving radial distance
	density = [] # Physical density
	# Scale factors
	a1 = []
	a1dot = []
	a2 = []
	a2dot = []
	# Hubble rates
	Ht = []
	Hr = []
	k = []
	pk = []
	pm = []
	tb = []
	tbprime = []

	gr = []
	gt = []
	gz = []
	gdm = []
	gzeds = []
	gdmeds = []
	gdl = []
	gdleds = []
	gdmmilne = []
	gHzr = []
	gHzt = []

	rr = []
	zz = []
	dz = []
	rend = []
	dens = []

	Rrr = []
	Rzz = []
	Rdz = []

	# Get data used for k(r)/tB(r) profiles and low-z Hubble rates
	f = open("../bubble/data/background-"+idd, 'r')

	for line in f.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 8:
			r.append(float(tmp[0]))
			density.append(float(tmp[1]))
			a1.append(float(tmp[2]))
			a1dot.append(float(tmp[3]))
			a2.append(float(tmp[4]))
			a2dot.append(float(tmp[5]))
			Ht.append(float(tmp[6]))
			Hr.append(float(tmp[7]))
			k.append(float(tmp[8]))
			pk.append(float(tmp[9]))
			pm.append(float(tmp[10]))
			tb.append(float(tmp[11]))
			tbprime.append(float(tmp[13]))
	f.close()

	# Get geodesic data, used for supernovae and H(z)
	g = open("../bubble/data/geodesic-"+idd, 'r')

	for line in g.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 10:
			gr.append(float(tmp[0]))
			gt.append(float(tmp[1]))
			gz.append(float(tmp[2]))
			gdm.append(float(tmp[3]))
			gzeds.append(float(tmp[4]))
			gdmeds.append(float(tmp[5]))
			gdl.append(float(tmp[6]))
			gdleds.append(float(tmp[7]))
			gdmmilne.append(float(tmp[8]))
			gHzr.append(float(tmp[9]))
			gHzt.append(float(tmp[10]))
	g.close()

	"""
	# Get vp(z) data
	h = open("../bubble/data/velocity-"+idd, 'r')

	for line in h.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 3:
			rr.append(float(tmp[0]))
			zz.append(float(tmp[1]))
			dz.append(float(tmp[2]))
			rend.append(float(tmp[5]))
			dens.append(float(tmp[8]))
	h.close()
	"""
	
	# Calculate shift params from the calc. model data (H's are in 100h units)
	sh = obs.obsParams(idd)
	S = 100.0 * sh.Hdec * sh.dAdec / (2.*C * ( (1.+sh.zdec)**0.5 - 1.) )
	H0eds = 100.0 * (1. + sh.zdec)**(-1.5) * sh.Hdec
	Hdec = sh.Hdec
	rlss = sh.rlss

	# Model info
	h = open("../bubble/data/modelinfo-"+idd, 'r')
	modname = h.readlines()[0][:-1]
	
	data = { 'r':r,	'density':density, 'a1':a1, 'a1dot':a1dot, 'a2':a2,
			 'a2dot':a2dot, 'Ht':Ht, 'Hr':Hr, 'k':k, 'pk':pk, 'pm':pm,
			 'tb':tb, 'tbprime':tbprime, 'gr':gr, 'gt':gt, 'gz':gz,
			 'gdm':gdm, 'gzeds':gzeds, 'gdmeds':gdmeds, 'gdl':gdl,
			 'gdleds':gdleds, 'gdmmilne':gdmmilne, 'Hzr':gHzr, 'Hzt':gHzt,
			 'rr':rr, 'zz':zz, 'dz':dz, 'rend':rend, 'dens':dens, 'Rrr':Rrr, 
			 'Rzz':Rzz, 'Rdz':Rdz, 'modname':modname, 'r_z':[],
			 'S':S, 'H0eds':H0eds, 'Hdec':Hdec, 'rlss':rlss }
	
	
	
	# Interpolate to get r(z) for first data set
	r_z = map(lambda x: zinterp(x, data), data['r']) # Interpolate for whole array
	data['r_z'] = r_z
	
	return data

