#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import numpy as np
import math, sys
#from hasty_settings import *

NBINS = 30
NLEVELS = 8
stot = 0

root = int(sys.argv[1])
nchains = int(sys.argv[2])
burnin = float(sys.argv[3])

def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]


A = []
for i in range(nchains):
	A.append( Ana.Analyse(root+i) )

#A = A[1:2]

# Make contour plots
vn = A[0].data_varnames
vn = ["Otot", "Ok", "Wk", "h"]
vn += ["S", "H0eds"]
for i in range(len(vn)):
	for j in range(len(vn)):
		if(j>i):
			# Collect data from all chains
			xx = []
			yy = []
			for a in A:
				xx += a.column(vn[i], lcut=burnin)
				yy += a.column(vn[j], lcut=burnin)
			
			print j, len(xx), len(yy)
			# Plot these data
			P.subplot(len(vn)-1, len(vn)-1, 1+(i)+(len(vn)-1)*(j-1))
			z, x, y = np.histogram2d(xx, yy, bins=NBINS)
			P.contourf(x[:-1], y[:-1], z.T, get_sig_levels(z)) ###NLEVELS)
			if(i==0):
				P.ylabel(vn[j], weight="bold", size="large")
			if(j==len(vn)-1):
				P.xlabel(vn[i], weight="bold", size="large")

# Get means for the variables
print ""
for var in vn:
	print "<" + str(var) + "> =",
	for a in A:
		print a.mean(var),
	print ""
print ""
P.show()
