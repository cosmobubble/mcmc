#!/usr/bin/python
"""Plot gamma(z) vs. H0"""

import numpy as np
import pylab as P
import scipy.stats as stats
from read_reduced_ksz import read_data

def gamma(v):
	"""Get the gamma factor for a velocity [v in 10^3 km/s]"""
	C = 3e2
	beta2 = v**2. / C**2.
	return 1. / np.sqrt(1. - beta2)

# Get vp(z) data for MCMC
z, vp, h = read_data()

# Filter-out high vp(z) values (above 200,000 km/s)
vp_low = []
z_low = []
for i in range(len(vp)):
	vp_low.append([])
	z_low.append([])
	for j in range(len(vp[i])):
		if vp[i][j] < 301:
			vp_low[i].append(vp[i][j])
			z_low[i].append(z[i][j])

# Get redshifts
zz = []
for i in range(len(z)):
	zz.append(z[i][0])

# Only use the lower redshifts, not much left if you only allow vp<200,000 at higher z!
z = z_low[:6]
vp = vp_low

# Plot gamma
gam = map(lambda x: map(lambda y: gamma(y), x), vp_low)
P.subplot(111)
for i in range(len(z)):
	P.plot(h[i], gam[i], marker=',', ls='None', label="z="+str(round(zz[i], 2)))
P.ylim((0.0, 10.0))
P.xlabel("H0")
P.ylabel("Gamma")
P.legend(prop={'size':'small'})
P.show()
