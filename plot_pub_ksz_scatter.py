#!/usr/bin/python
"""Graph for publication. beta(z) vs. z for z<0.3, with 1sigma and 2sigma CL bounds.
   The data are from runs 1100(8), which have Atb!=0 and the full CMB+H0+Sne constraints
   (although the Sne are pre-Union2, using Ned Wright's binned distance moduli)
   Note that samples with beta > 200x10^3 km/s have been trimmed out to get the bounds"""

import numpy as np
import pylab as P
import scipy.stats as stats
from read_reduced_ksz import read_data
from fast_kde import fast_kde

NBINS = 20

def get_centroids(vals):
	"""Get bin centroids"""
	cent = []
	for i in range(len(vals)-1):
		cent.append(0.5*(vals[i]+vals[i+1]))
	return cent

def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	#lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	lvls = [0.0, 0.68, 0.95, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]

# Get vp(z) data from MCMC
z, vp, h = read_data()

# Filter-out high vp(z) values (above 200,000 km/s) [N.B. CHANGED TO 300,000 km/s]
MAX_BETA = 200. #301. # 200.
vp_low = []
z_low = []
h_low = []
for i in range(len(vp)):
	vp_low.append([])
	z_low.append([])
	h_low.append([])
	for j in range(len(vp[i])):
		if vp[i][j] < MAX_BETA:
			vp_low[i].append(vp[i][j]/300.) # Convert to units of dT/T
			z_low[i].append(z[i][j])
			h_low[i].append(h[i][j])

# Only use the lower redshifts, not much left if you only allow vp<200,000 at higher z!
z = z_low
vp = vp_low
h = h_low

# Upper and lower percentiles
cols = ['k', 'r', 'b', 'g', 'c', 'y']

# Make the plot
P.subplot(111)
BINS=90
zidx = [1, 2, 3, 4] # Only plot z=0.05, 0.1, 0.15, 0.2
# Bug fixed 2011-10-26: Wrong labels were being used. Fig. 7 in arXiv:1108.2222v1.
zlbls = ["z=0.05", "z=0.1", "z=0.15", "z=0.2"]
ylbl = [0.21, 0.36, 0.53, 0.74]

j = 0
for i in zidx:
	f, x, y = np.histogram2d(h[i], vp[i], bins=NBINS)
	#P.contour(get_centroids(x), get_centroids(y), f.T, get_sig_levels(f), colors=cols[j])
	#P.plot(h[i], vp[i], 'y,')
	
	# Get KDE-smoothed Gaussians
	kde_grid = fast_kde( h[i], vp[i], gridsize=(BINS, BINS) )
	xpts = np.linspace(min(h[i]), max(h[i]), BINS)
	ypts = np.linspace(max(vp[i]), min(vp[i]), BINS)
	P.contour(xpts, ypts, kde_grid, get_sig_levels(kde_grid), colors=cols[j], linewidths=1.5)
	P.figtext(0.75, ylbl[j], zlbls[j], fontsize=20)
	j += 1

# Set font size
fontsize = 20.
ax = P.gca()
kk = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
for tick in ax.yaxis.get_major_ticks():
	kk += 1
	tick.label1.set_fontsize(fontsize)
	if kk%2==0:
		tick.label1.set_visible(False)

# Get rid of the first tick
ax.yaxis.get_major_ticks()[0].label1.set_visible(False)

P.xlim((0.65, 0.85))

# Axis labels
P.xlabel(r"h", fontdict={'fontsize':'24'})
P.ylabel("$\Delta T / T$", fontdict={'fontsize':'24'})

P.show()
