#!/usr/bin/python
"""Get likelihoods for all of the observables for a given model."""

import hasty_constraints as constraint
import hasty_data as dat
import sys

# ID of Bubble run data files
if len(sys.argv) > 1:
	idd = int(sys.argv[1])
else:
	idd = 10

# Load the supernova data files
preloaded_sne_data = dat.sne_data_union2() # Load observational SNe data

# Get the chi-squared likelihoods from the models
l_sne, sn_offset = constraint.supernovae_union2(idd, preloaded_sne_data)
l_h0 = constraint.local_hubble_rate(idd)
l_cmb = constraint.cmb_shift(idd)
l_ksz = constraint.ksz(idd)

print " "
print "Likelihoods for model id =", idd
print "SN:\t", round(l_sne, 3)
print "H0:\t", round(l_h0, 3)
print "CMB:\t", round(l_cmb, 3)
print "KSZ:\t", round(l_ksz, 3)
print " "
print "(See comments in this script for important information.)"

# NOTE 1: The function constraint.ksz() will naively read-in a full kSZ data 
# set (i.e. one produced in Bubble by calling output_velocities()) and 
# interpret it as if it were the reduced data set produced by calling 
# output_sparse_velocities(). This makes it give the wrong kSZ chi-squared 
# (i.e. much larger). Make sure that the velocity data files being read in 
# were produced with output_sparse_velocities().
