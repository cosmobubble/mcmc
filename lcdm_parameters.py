#!/usr/bin/python

import scipy.integrate as S
import math

class Model(object):

	def __init__(self, H0, Om, Ol, zeq=3148.):
		self.Om = Om # Matter density
		self.Or = Om / (1. + zeq)
		self.Ol = 1. - (self.Om + self.Or) # Dark energy density
		self.H0 = H0 # Hubble's constant, in km/sec/Mpc
		self.zeq = zeq # Redshift at equality
		self.C = 3e5 # Speed of light in km/sec

	def H(self, z):
		"""Hubble rate for a given z"""
		return self.H0 * math.sqrt(self.Om*(1.+z)**3.+self.Or*(1.+z)**4.+self.Ol)

	def dl_integrand(self, z):
		"""The integrand for the integration required to evaluate dL(z)"""
		return 1. / math.sqrt(self.Om*(1.+z)**3.+self.Or*(1.+z)**4.+self.Ol)

	def dL(self, z):
		"""Find the luminosity distance in LCDM for a given redshift z [dL in Mpc]"""
		return (1. + z) * (self.C/self.H0) * S.romberg(self.dl_integrand, 0.0, z, divmax=100)

	def dA(self, z):
		"""Find the angular diameter distance in LCDM for a given redshift z [dA in Mpc]"""
		return self.dL(z)/(1.+z)**2.
	
	def deltaDM(self, z):
		"""Find the difference in distance modulus between a Milne universe and LCDM"""
		return 5. * math.log10( self.dL(z) / ((self.C/self.H0) * (z + 0.5*z**2.)) )
	
	def Shift(self, z):
		"""Shift parameter with respect to an EdS universe"""
		return (self.H(z)*self.dA(z)*0.5/self.C) / (math.sqrt(1.+z) - 1.)
	
	def dH(self, z):
		"""Hubble rate shift w.r.t. EdS universe"""
		return (self.H(z)*(1.+z)**-1.5)/self.H0
