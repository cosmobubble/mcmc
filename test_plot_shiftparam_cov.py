#!/usr/bin/python

import pylab as P
import numpy as np
import numpy.random as nran
import math
import hasty_data

NBINS = 50
NLEVELS = 8
stot = 0


def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]

# Get shift params from data file
shdat = hasty_data.shift_params() # Load Shift parameter *data*
S = shdat['S']
dH = shdat['dH']
u = [S, dH]
cov = shdat['cov']

# Make contour plots
v = nran.multivariate_normal(u, cov, 500000)
xx = map(lambda x: x[0], v)
yy = map(lambda x: x[1], v)

# Plot these data
P.subplot(111)
z, x, y = np.histogram2d(xx, yy, bins=NBINS)
P.contour(x[:-1], y[:-1], z, get_sig_levels(z))
P.xlabel('S')
P.ylabel('dH')

# Plot best-fits
##S = 1.02283350099
##dH = 0.724215511775
##H0 = 65.4
##H = H0 * dH # H_eds

# WMAP best-fit LCDM
P.plot(0.888, 0.515, 'ro', label="WMAP bestfit LCDM, $\chi^2=$"+str(77.92)+ ", l=" + str(81.24))
P.plot(1.02283350099, 0.724215511775, 'bo', label="Void MCMC bestfit, $\chi^2=$"+str(1134)+ ", l=" + str(20.05))
P.legend(loc='upper left')

P.show()
