#!/usr/bin/python
"""Calculate observables for a model which was accepted by the MCMC chain"""

import subprocess
import hasty_analysis
import sys, os, signal, time
from hasty_settings import *

def run_model(params, uid):
	"""Run model with the parameters in params"""
	args = []
	args.append(EXEC_PATH)
	params = map(lambda x: str(round(x, 4)), params)
	
	# Run a model with extra argument UID (means it outputs kSZ etc.)
	args = args + params + [str(999)] + [str(uid)]
	
	# Run process and wait to see if it finishes in the allowed time
	proc = subprocess.Popen(args, cwd=WORKING_DIR)
	tstart = time.time()
	while proc.poll() is None:
		time.sleep(0.1)
		if time.time() - tstart > TIMEOUT:
			# Process has been running for too long
			print "WARNING: Model has been running for too long - killing it."
			os.kill(proc.pid, signal.SIGKILL)
			os.waitpid(-1, os.WNOHANG)
			return False
		
	# Get the status of the process
	status = proc.poll()
	if status != 0:
		print "Model run failed with status", status
		return False
	else:
		return True

chainid = int(sys.argv[1])
print "Working on chain", chainid
A = hasty_analysis.Analyse(chainid)
uid = chainid*100000
oldtime = time.time()

# Loop through each accepted step in the chain, doing the full analysis
for s in A.chain:
	uid += 1
	print "\n(" + str(uid) + ") Running..."
	
	# Build the parameter vector for this step and run the model
	params = [ s['Otot'], s['Ok'], s['Wk'], s['Atb'], s['Wtb'], s['h']  ]
	success = run_model(params, uid)
	
	print "\tRun took", round(time.time() - oldtime, 2), "sec"
	oldtime = time.time()
	
	if not success:
		print "\tFailed, with parameters", params
