#!/usr/bin/python
"""
Bring together output from the initial MCMC run and the follow-up run which 
calculated more CPU-intensive observables like kSZ.
"""

import hasty_analysis
import scipy.interpolate
import numpy as np
import pylab as P
from hasty_settings import *

class AnalyseObs(object):

	def __init__(self, CHROOT, NCHAINS):
		"""Process all of the new data and package it ready for analysis"""
		self.chain = [] # New composite list of data
		
		# Get analysers for each of the chains
		A = []
		for i in range(NCHAINS):
			A.append( hasty_analysis.Analyse(CHROOT+i) )

		# Construct new dictionaries for data
		z = np.arange(0.0, 1.0, 1e-2)
		e = 0
		for i in range(NCHAINS):
			self.chain.append([])
			j = 0
			for s in A[i].chain:
				j += 1
				uid = 100000*(CHROOT+i) + j
				try:
					ddm, lss, ksz, stat = self.get_new_data(uid)
					# Add the new data structure to the chain
					d = dict( A[i].chain[j] )
					d2 = { "obsDM":ddm, "obsLSS":lss, "obsKSZ":ksz, 
					       "obsSTAT":stat, "uid":uid }
					d.update(d2)
					self.chain[i].append( d )
				# Warning: Python 2.x syntax. In Python 3 this would be "except Exception as msg"
				except Exception, msg:
					if "No such file or directory" not in msg:
						e += 1
					pass
		print "*** Error count:", e
		
	
	def get_ddm_data(self, lines):
		"""Strip-out the deltaDM data from an mcmcobs file"""
		complete = False
		dDM = []
		for i in range(len(lines)):
			# Check for data divider
			if "----- Finished dDM" in lines[i]:
				complete = True
				break
		
			# Strip data from line
			tmp = lines[i].split("\t")
			_r = float(tmp[0])
			_z = float(tmp[1])
			_dm = float(tmp[2])
			_dl = float(tmp[3])
			dDM.append( { "r":_r, "z":_z, "dDM":_dm, "dL":_dl } )
		return dDM, complete

	def get_lss_data(self, lines):
		"""Strip-out the LSS data from an mcmcobs file"""
	
		# Get ID of LSS line
		a = 0
		b = 0
		for i in range(len(lines)):
			if "----- Finished dDM" in lines[i]:
				a = i
			if "----- Finished LSS" in lines[i]:
				b = i
		i = b-1
		if i == -1:
			# Didn't find it, there was an output problem
			return {}, False
	
		# Process line
		tmp = lines[i].split("\t")
		dA = float(tmp[0])
		Hlss = float(tmp[1])
		rho = float(tmp[2])
		m = float(tmp[3])
		k = float(tmp[4])
		H0 = float(tmp[5])
		rlss = float(tmp[6])
		tlss = float(tmp[7])
	
		lss = { "dA":dA, "Hlss":Hlss, "rho":rho, "m":m, "k":k, "H0":H0, 
			    "rlss":rlss, "tlss":tlss }
		return lss, True

	def get_ksz_data(self, lines):
		"""Strip-out the kSZ data from an mcmcobs file"""
		ksz = []
		a = -1
		b = -1
		for i in range(len(lines)):
			if "----- Finished LSS" in lines[i]:
				a = i
			if "----- Finished kSZ" in lines[i]:
				b = i
		if a == -1:
			# Didn't find the start of the kSZ data, there was an output problem
			"WARNING: No kSZ data whatsoever!"
			return ksz, False
	
		# Strip the data
		for line in lines[a:]:
			tmp = line.split("\t")
			if len(tmp) >= 5:
				_r = float(tmp[0])
				_z = float(tmp[1])
				_vp = float(tmp[2])
				_rin = float(tmp[3])
				_tin = float(tmp[4])
				ksz.append( { "r":_r, "z":_z, "vp":_vp, "rin":_rin, "tin":_tin } )
	
		# Check if the full kSZ output was finished
		if b == -1:
			return ksz, False
		else:
			return ksz, True

	def get_new_data(self, uid):
		"""Process the data file for a given chain and UID, and return the data"""
		f = open( WORKING_DIR + "data/mcmcobs-" + str(uid) )
		lines = f.readlines()
		f.close()
	
		# Get different data sets
		dDM, s_dDM = self.get_ddm_data(lines)
		lss, s_lss = self.get_lss_data(lines)
		ksz, s_ksz = self.get_ksz_data(lines)
		status = [s_dDM, s_lss, s_ksz]
	
		return dDM, lss, ksz, status

	def interpolate_ksz(self, ksz, zz):
		"""Get an interpolated value for the ksz at some z"""
		vp = map( lambda x: x['vp'], ksz )
		z = map( lambda x: x['z'], ksz )
		
		if len(vp) < 5 or z[-1] < 0.3:
			raise Exception('BadKSZdata', 'The kSZ data were not complete.')
		
		spline = scipy.interpolate.UnivariateSpline(z, vp, k=5)
		return spline(zz)[0]
