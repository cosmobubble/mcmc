#!/usr/bin/python
"""Read-in the reduced ksz file"""

import numpy as np
import pylab as P

def read_data(fname="ksz-data-1100"):
    """Read-in the reduced kSZ data from the filename given"""

    # Read-in the data
    f = open(fname, 'r')
    dat = []
    for line in f.readlines():
        tmp = line.split("\t")
        if len(tmp) > 3:
            for i in range(len(tmp)):
                tmp[i] = float(tmp[i])
            dat.append(tmp)
    #dat = np.genfromtxt(fname, unpack=True)
    #print dat[0]

    # Reduce into a single-column form; each z sample takes up one column
    vp = []
    z = []
    for i in range(10):
        vp.append([])
        z.append([])

    # For each record, split-out data
    for i in range(len(dat)):
        nsamples = int(dat[i][1])
        # Separate interleaved z/vp data
        for j in range(nsamples):
            x = 2*(j+1)
            z[j].append( dat[i][x] )
            vp[j].append( dat[i][x+1] )

    return z, vp

z, vp = read_data()

P.subplot(111)
for i in range(len(z)):
    P.plot(z[i], vp[i], ls="None", marker=",")
P.show()
