#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import numpy as np
import math
from hasty_settings import *
import sys

ROOT = int(sys.argv[1])
NCHAINS = int(sys.argv[2])
BURNIN = float(sys.argv[3])

A = []
for i in range(NCHAINS):
	A.append( Ana.Analyse(ROOT+i) )

vn = A[0].data_varnames
cols = ['r', 'y', 'k', 'g', 'y', 'c', 'm']
bbins = []

# Plot distributions for each variable for combined chains
for i in range(len(vn)):
	P.subplot(231+i)
	P.title(vn[i])
	xx = []
	for j in range(len(A)):
		xx += A[j].column(vn[i], lcut=BURNIN)
	
	n, bins, patches = P.hist(xx, 20, normed=False, fc='gray', ec='gray')
	bbins.append(bins)
	#y, x = np.histogram(xx, bins=20, normed=True)
	#P.plot(x[:-1], y, cols[i]+"-")

# Plot separate distributions for each variable for each chain
for j in range(len(A)):
	for i in range(len(vn)):	
		P.subplot(231+i)
		P.title(vn[i])
		
		#n, bins, patches = P.hist(A[j].column(vn[i]), 20, normed=1, facecolor=cols[i])
		y, x = np.histogram(A[j].column(vn[i], lcut=BURNIN), bins=bbins[i], normed=False)
		P.plot(x[1:], y, cols[j]+"-", drawstyle='steps', linewidth=1.1)

P.show()
