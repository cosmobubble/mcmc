#!/usr/bin/python
"""Check the status of a currently-running MCMC"""

import sys, time
from hasty_settings import *

# Define status tags
tags = ["ACCEPTED", "REJECTED", "RUN FAILED", "CLOSE TO LOCAL", "CLOSE TO KSZ"]
short_tags = ['*', '-', 'x', '_', '>']
prev = 0

try:
	CHROOT = int(sys.argv[1])
	NCHAINS = int(sys.argv[2])
except:
	print "ERROR: Need to specify (1) Chain root number and (2) No. of chains"
	exit()

# Count previous statuses
lengths = []
for i in range(NCHAINS):
	lengths.append(0)

# Run interrupted by user
while True:
	# Prepare string list of statuses
	statuses = []
	
	# Prepare counting array
	tagcount = []
	for tag in tags:
		tagcount.append(0)
	
	# Get data from all the logfiles
	for k in range(NCHAINS):
		statuses.append("")
		f = open("log-" + str(CHROOT+k), 'r')
		for line in f.readlines():
			for i in range(len(tags)):
				if tags[i] in line:
					tagcount[i] += 1
					statuses[k] += short_tags[i]
		f.close()
		
	# Output information
	tot = sum(tagcount)
	print "-"*50
	for i in range(len(tags)):
		print tags[i], "\t", tagcount[i], "\t", round(100.*float(tagcount[i])/float(tot)), "%"
	print "TOTAL", "\t\t", tot, "\t+" + str(tot - prev)
	prev = tot
	print "Acceptance rate \t", round(100.*float(tagcount[0]) / float(tagcount[0]+tagcount[1])), "%"
	
	# Print chain history
	for k in range(NCHAINS):
		delta = len(statuses[k]) - lengths[k]
		lengths[k] = len(statuses[k])
		if len(statuses[k])>60:
			print "(" + str(CHROOT+k) + ")", statuses[k][-60:], "(+"+str(delta)+")"
		else:
			print "(" + str(CHROOT+k) + ")", statuses[k], "("+str(delta)+")"
	
	# Wait until doing it again
	time.sleep(15)


"""
		print "-"*50, "\nRUN", CHAIN_ROOT+k
		tot = sum(tagcount)
		for i in range(len(tags)):
			print tags[i], "\t", tagcount[i], "\t", round(100.*float(tagcount[i])/float(tot)), "%"

		print "Acceptance rate \t", round(100.*float(tagcount[0]) / float(tagcount[0]+tagcount[1])), "%"
		"""
