#!/usr/bin/python

"""Handle observable variables and calculate a likelihood"""

import scipy.interpolate as interp
import subprocess
import pylab as P
import numpy as np

class deltaDM(object):
	"""Load deltaDM's from a void model output file and make them available 
	   as a spline"""
	
	def __init__(self, idd):
		self.filename = "../bubble/data/mcmc_deltaDM-" + str(idd)
		self.load_data()
	
	def load_data(self):
		"""Open the file of model data points, parse it, and load the data"""
		# Prepare the data arrays
		self.z = []
		self.deltaDM = []
		
		# Read-in the file
		f = open(self.filename, 'r')
		for line in f.readlines():
			tmp = line.split("\t")
			if len(tmp) >= 2:
				self.z.append(float(tmp[0]))
				self.deltaDM.append(float(tmp[1]))
		f.close()
		
		# Correct 1st value - probably NaN
		self.z[0] = 0.0
		self.deltaDM[0] = 0.0
		
	
	def DM(self, zz):
		"""Get an interpolated deltaDM(z) value from the dataset"""
		
		# Find the z array value that is closest to z
		ii = 0
		minval = 1e5 # Set to large number initially
		for i in range(len(self.z)):
			diff = abs(self.z[i] - zz)
			if diff < minval:
				minval = diff
				ii = i
		
		# Find a slice of z array values near to the z value (5 values)
		if(ii <= 2):
			# Near the start of the array
			imin = 0
			imax = 4
		elif(ii >= len(self.z) - 3):
			# Near the end of the array
			imin = len(self.z) - 5
			imax = len(self.z) - 1
		else:
			# Somewhere in the middle
			imin = ii - 2
			imax = ii + 2
		
		# Build a spline using this slice of the arrays
		sp = interp.UnivariateSpline(self.z[imin:imax], self.deltaDM[imin:imax], k=3)
		return sp(zz)

class obsParams(object):
	"""Get various basic parameters calculated from the model"""
	
	def __init__(self, idd):
		self.filename = "../bubble/data/mcmc_modeldata-" + str(idd)
		self.load_data()
	
	def load_data(self):
		"""Open the file, parse it, and load the data"""
		
		# Init variables
		self.H0 = 0.0
		self.Hdec = 0.0
		self.dAdec = 0.0
		self.zdec = 0.0
		self.zlss = 0.0
		self.ksz_lss_flatness = 0.0
		self.r_asym_lss = 0.0
		self.rlss = 0.0
		self.tlss = 0.0
		self.lss_density_contrast = 0.0
		self.local_lss_flatness = 0.0
		
		# Read-in the file
		f = open(self.filename, 'r')
		numrec = 0
		for line in f.readlines():
			tmp = line.split("\t")
			if len(tmp) >= 9:
				numrec += 1
				self.H0 = (float(tmp[0]))
				self.Hdec = (float(tmp[1]))
				self.dAdec = (float(tmp[2]))
				self.zdec = (float(tmp[3]))
				self.zlss = (float(tmp[4]))
				self.ksz_lss_flatness = (float(tmp[5]))
				self.r_asym_lss = (float(tmp[6]))
				self.rlss = (float(tmp[7]))
				self.tlss = (float(tmp[8]))
				self.lss_density_contrast = (float(tmp[9]))
				self.local_lss_flatness = (float(tmp[10]))
		f.close()
		
		# Throw an error if something isn't right
		if numrec == 0:
			Exception("hasty_observables::obsParams(): No data were found!")

class kszParams(object):
	"""Get the kSZ vp(z) calculated from the model"""

	def __init__(self, idd):
		self.fname = "../bubble/data/velocity-" + str(idd)
		self.load_data()
	
	def load_data(self):
		"""Load the data from the velocity file"""
		f = open(self.fname, 'r')
		rr = []
		zz = []
		vp = []
		for line in f.readlines():
			tmp = line.split("\t")
			if len(tmp) >= 3:
				#rr.append(float(tmp[0]))
				zz.append(float(tmp[1]))
				vp.append(float(tmp[2]))
		f.close()
		
		self.z = zz
		self.vp = vp
