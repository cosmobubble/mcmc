#!/usr/bin/python

import hasty_analysis
import sys
import pylab as P

root = int( sys.argv[1] )
num = int( sys.argv[2] )

# Get analysis objects
A = []
for i in range(num):
	A.append( hasty_analysis.Analyse(root + i) )

# Get only bad lss 
badlss = []
accepted = []
rejected = []
bad = []
for i in range(len(A)):
	for j in range(len(A[i].steps)):
		thisrej = A[i].steps[j]['rej']
		if thisrej == 'p':
			badlss.append(A[i].steps[j])
		if thisrej == 'a':
			accepted.append(A[i].steps[j])
		if thisrej == 'r':
			rejected.append(A[i].steps[j])
		if thisrej == 'b':
			bad.append(A[i].steps[j])

def get_column(data, vname):
	"""Get a column from a dict"""
	rows = []
	for item in data:
		rows.append(item[vname])
	return rows

def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]



# Make plots
vn = A[0].data_varnames
#vn += ["S", "H0eds"]
for i in range(len(vn)):
	for j in range(len(vn)):
		if(j>i):
			# Collect data from all chains
			xxl = get_column(badlss, vn[i])
			yyl = get_column(badlss, vn[j])
			xxa = get_column(accepted, vn[i])
			yya = get_column(accepted, vn[j])
			xxr = get_column(rejected, vn[i])
			yyr = get_column(rejected, vn[j])
			xxb = get_column(bad, vn[i])
			yyb = get_column(bad, vn[j])
			
			# Plot these data
			P.subplot(len(vn)-1, len(vn)-1, 1+(i)+(len(vn)-1)*(j-1))
			#z, x, y = np.histogram2d(xx, yy, bins=NBINS)
			#P.contourf(x[:-1], y[:-1], z, get_sig_levels(z)) ###NLEVELS)
			
			P.plot(xxr, yyr, 'y,')
			P.plot(xxa, yya, 'k,')
			P.plot(xxb, yyb, 'r,')
			P.plot(xxl, yyl, 'b,')
			
			if i==1 and j==2:
				print "-"*50
				print "Rej Acc Bad LSS"
				print len(xxr), len(xxa), len(xxb), len(xxl)
				print "y   k   r   b"
				print "-"*50
			
			
			
			if(i==0):
				P.ylabel(vn[j], weight="bold", size="large")
			if(j==len(vn)-1):
				P.xlabel(vn[i], weight="bold", size="large")

P.show()
