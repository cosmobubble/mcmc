#!/usr/bin/python
"""Calculate shift parameters relative to an effective EdS model with radiation"""

import math
C = 3e5 # Speed of light, km/s

def OmegaR(zeq):
	"""Radiation density relative to today in effective EdS model, given zeq"""
	return 1. / (2. + zeq)

def OmegaM(zeq):
	"""Matter density in eff. EdS model, given zeq (which fixes the rad. density)"""
	return (1. + zeq) / (2. + zeq)

def EffectiveH0(Hstar, zstar, zeq):
	"""Hubble param today of effective EdS model which has H=H* at z=z*, km/s/Mpc"""
	Om = OmegaM(zeq)
	Or = OmegaR(zeq)
	zz = (1. + zstar)
	H0 = Hstar / math.sqrt( Om*zz**3. + Or*zz**4. )
	return H0

def Shift(dA, Hstar, zstar, zeq):
	"""Shift parameter S = dA(z*) / dA(z*)|EdS+rad. Takes dA(model, z*), Hubble 
	   rate at z* Hstar, redshift of LSS z*, redshift of equality zeq"""
	Om = OmegaM(zeq)
	Or = OmegaR(zeq)
	H0 = EffectiveH0(Hstar, zstar, zeq)
	S = dA * H0 * math.sqrt(Om) / ( 2.*C*( 1. - math.sqrt( (Om/(1.+zstar)) + Or) ) )
	return S

def dH(H0, Hstar, zstar, zeq):
	"""Shift parameter dH = H0|Eds / H0|model. Arg. H0 is H0|model"""
	H0_eds = EffectiveH0(Hstar, zstar, zeq)
	dH = H0_eds / H0
	return dH
