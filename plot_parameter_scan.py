#!/usr/bin/python

from pylab import *
import sys, time, os, subprocess
import hasty_constraints as HC
import hasty_data as HD
import get_bubble_data
from hasty_settings import *
import numpy as np

tstart = time.time()
ZMAX = 6.
idd = 99
cols = ['k-', 'r-', 'b-', 'g-', 'c-', 'm-', 'y-']
cols += cols + cols + cols

# Get the Ned Wright SNe data
sne = HD.sne_data_wright()
sneZ = map(lambda x: x['z'], sne)
sneDM = map(lambda x: x['dm'], sne)
sneERR = map(lambda x: x['err'], sne)

# Get the best-fit CMB data
bfS = HD.shift_params()['S']
bfH0eds = HD.shift_params()['H0eds']
bfCov = HD.shift_params()['cov']
bfHdec = bfH0eds * 1091**1.5

def run_model(idd, params):
	"""Run model with the parameters in params"""
	#print "\tWorking..."
	args = []
	args.append(EXEC_PATH)
	params = map(lambda x: str(round(x, 4)), params)
	# Add extra two arguments [0,0] so it does the full Bubble run
	args = args + params + [str(idd)] + ['0', '0']
	for prm in params:
		print prm,
	print ""
	
	# Run process and wait to see if it finishes in the allowed time
	proc = subprocess.Popen(args, cwd=WORKING_DIR, stderr=subprocess.PIPE)
	stderror = proc.communicate() # Standard output
	
	tstart = time.time()
	while proc.poll() is None:
		time.sleep(0.1)
		if time.time() - tstart > TIMEOUT:
			# Process has been running for too long
			print "FAIL"
			print "\tERROR: Model running for too long, killing it."
			os.kill(proc.pid, signal.SIGKILL)
			os.waitpid(-1, os.WNOHANG)
			return False
	
	# Get the status of the process
	status = proc.poll()
	if status != 0:
		#print "FAIL"
		#print "\tModel run failed with status", status
		print "\tError report:", stderror
		return False
	else:
		print "AOK"
		return True

################################################################################
# Scan desired parameter space
lbls = []
density = []
r_z = []
tb = []
tbprime = []
k = []
Ht = []
Hr = []
gz = []
gdm = []
gHzr = []
gHzt = []
rlss = []
Hdec = []
S = []
H0eds = []

gr = []
gt = []

# DEFAULT PARAMS
N = 2
#p = [0.86, 0.9, 3400.55, 0.0, 10.0, 0.7]
#p = [0.84, 0.80, 2400.0, -2000.0, 4000.0, 0.7]
###p = [0.82, 0.85, 2400.0, 3000.0, 500.0, 0.6]
##p = [0.901, 0.935, 3680.0, 6716.0, 1333.0, 0.7]
# Tim's best (approx): p = [0.61, 0.66, 1800.0, 1000.0, 4000.0, 0.7]
####p = [0.6, 0.8, 3000., 0., 3000., 0.7]

#p = [0.865, 0.814, 1738., -171., 3324., 0.7]

pp = []
#pp.append( [0.85, 0.85, 1800., 0.0, 4000., 0.7] )
#pp.append( [0.85, 0.85, 1800., 1500.0, 4000., 0.7] )
#pp.append( [0.85, 0.85, 1800., -1500.0, 4000., 0.7] )

###########pp.append( [-0.9, -0.7, 4000., 3000.0, 1000., 0.7] )
pp.append( [0.65, 0.65, 2000., 0.0, 1000., 0.5] )
pp.append( [0.90, 0.90, 2000., 0.0, 1000., 0.5] )
pp.append( [0.65, 0.65, 2000., -2000.0, 1000., 0.5] )
pp.append( [0.90, 0.90, 2000., -2000.0, 1000., 0.5] )

pp.append( [0.65, 0.65, 2000., 0.0, 1500., 0.5] )
pp.append( [0.90, 0.90, 2000., 0.0, 1500., 0.5] )
pp.append( [0.65, 0.65, 2000., -2000.0, 1500., 0.5] )
pp.append( [0.90, 0.90, 2000., -2000.0, 1500., 0.5] )

pp.append( [0.65, 0.65, 2000., 0.0, 2000., 0.5] )
pp.append( [0.90, 0.90, 2000., 0.0, 2000., 0.5] )
pp.append( [0.65, 0.65, 2000., -2000.0, 2000., 0.5] )
pp.append( [0.90, 0.90, 2000., -2000.0, 2000., 0.5] )


#for i in range(N):
for i in range(len(pp)):
	p = pp[i]
	lbl = "Wtb=" + str(p[4])
	if not run_model(idd, p):
		print "MODEL RUN FAILED"
		pass
	else:
		#d = get_bubble_data.fetch(this_idd[i])
		d = get_bubble_data.fetch(idd)
		lbls.append(lbl)
		
		density.append(d['density'])
		r_z.append(d['r_z'])
		tb.append(d['tb'])
		tbprime.append(d['tbprime'])
		k.append(d['k'])
		Ht.append(d['Ht'])
		Hr.append(d['Hr'])
		gz.append(d['gz'])
		gdm.append(d['gdm'])
		gHzr.append(d['Hzr'])
		gHzt.append(d['Hzt'])
		rlss.append(d['rlss'])
		Hdec.append(d['Hdec'])
		S.append(d['S'])
		H0eds.append(d['H0eds'])
		
		gr.append(d['gr'])
		gt.append(d['gt'])
		
	#p[1] += 0.08


# Density profile
subplot(111)
for i in range(len(lbls)):
	plot(r_z[i], density[i], cols[i], label=str(lbls[i]))
grid(True)
title('$\\rho(r)$')
xlim(0.0, ZMAX)
legend()

"""
################################################################################
# Likelihoods

print "\n", d['modname'], "\n"

Lsne = HC.supernovae(idd)
Lcmb = HC.cmb_shift(idd)
Lh0 = HC.local_hubble_rate(idd)
Lksz = HC.ksz_bang_time_proximity(idd, maxgrad=1e-10)[0]
Ltb = HC.local_bang_time_proximity(idd, maxgrad=1e-10)
L = Lsne + Lcmb + Lh0 + Lksz + Ltb

Lstring = ""
Lstring += "Sne:    " + str(round(Lsne, 2)) + "\n"
Lstring += "CMB:    " + str(round(Lcmb, 2)) + "\n"
Lstring += "H0:     " + str(round(Lh0, 2)) + "\n"
Lstring += "kSZ:    " + str(round(Lksz, 2)) + "\n"
Lstring += "tB:     " + str(round(Ltb, 2)) + "\n"
Lstring += "TOTAL:  " + str(round(L, 2))
"""


################################################################################
# Plots
#figtext(0.75, 0.15, Lstring)

# Density profile
subplot(241)
for i in range(len(lbls)):
	plot(r_z[i], density[i], cols[i], label=lbls[i])
grid(True)
title('$\\rho(r)$')
xlim(0.0, ZMAX)

# Bang time profile
subplot(242)
for i in range(len(lbls)):
	plot(r_z[i], tb[i], cols[i], label=lbls[i])
grid(True)
title('tB(r)')
xlim(0.0, ZMAX)
legend()

# Bang time radial derivative profile
subplot(243)
for i in range(len(lbls)):
	plot(r_z[i], tbprime[i], cols[i])
grid(True)
title('tB\'(r)')
xlim(0.0, ZMAX)


subplot(244)
for i in range(len(lbls)):
	plot(gr[i], gt[i], cols[i], label=lbls[i])
grid(True)
title('t(r)')
#xlim(0.0, ZMAX)


"""
# Curvature profile
subplot(244)
for i in range(len(lbls)):
	plot(r_z[i], k[i], cols[i], label=lbls[i])
grid(True)
title('k(r)')
xlim(0.0, ZMAX)
"""
"""
# Expansion rates
subplot(245)
for i in range(len(lbls)):
	plot(r_z[i], Ht[i], cols[i], label=lbls[i])
	plot(r_z[i], Hr[i], cols[i]+"-", label=lbls[i])
grid(True)
title('Hubble Rates')
xlim(0.0, ZMAX)

"""
# Expansion rates H(z)
subplot(245)
zzz = np.arange(0.0, 10.0, 0.01)
hlcdm = map( lambda xx: 0.7*np.sqrt(0.3*(1.+xx)**3. + 0.7), zzz )
plot(zzz, hlcdm, 'y-')
for i in range(len(lbls)):
	plot(gz[i], gHzr[i], cols[i], label=lbls[i])
title('H(z)')
grid(True)


# Supernovae
subplot(246)
errorbar(sneZ, sneDM, yerr=sneERR, fmt="y.")
for i in range(len(lbls)):
	plot(gz[i][1:], gdm[i][1:], cols[i], label=lbls[i])
grid(True)
title('deltaDM(z)')
xlabel('z')
xlim(0.0, ZMAX)

# Hubble rates H_LSS
subplot(247)
errorbar(bfH0eds, 1e-2*bfHdec, xerr=np.sqrt(bfCov[1][1]), yerr=1e-2*np.sqrt(bfCov[1][1])*(1091**1.5), fmt='rx')
for i in range(len(lbls)):
	plot(H0eds[i], Hdec[i], cols[i][0]+'o')
	#print "\t", i, rlss[i], round(H0eds[i],1), round(1e-2*H0eds[i]*1091**1.5, 0), round(Hdec[i],0), round(1e-2*bfHdec, 0)
grid(True)
title('Large z Hubble rate')
xlabel('H0eds')
ylabel('H_LSS')
#xlim(0.0, ZMAX)

# CMB shift parameters
subplot(248)
errorbar(bfS, bfH0eds, fmt='rx', xerr=np.sqrt(bfCov[0][0]), yerr=np.sqrt(bfCov[1][1]) )
for i in range(len(lbls)):
	plot(S[i], H0eds[i], cols[i][0]+'o', label=lbls[i])
grid(True)
title('CMB Shift')
xlabel('S')
ylabel('H0eds')

print "Finished parameter scan in", round(time.time() - tstart, 1), "seconds."
show()
