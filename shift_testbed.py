#!/usr/bin/python
"""Shift parameters defined w.r.t. a locally-FRW void model"""

import math as m
c = 3e5 # km/s

Ok = 0.73
Om = 1. - Ok
H0 = 72. # Local Hubble parameter in km/s/Mpc
zs = 1090.

#dA_LTB = 9597.97 # Actual LTB model
dA_LTB = 14100.55 # LCDM

def r(z):
	"""Some comoving radial coordinate in open FRW model"""
	return m.asinh( m.sqrt(Ok/Om) ) - m.asinh( m.sqrt( Ok/((1.+z)*Om) ) )

def dA_k(z):
	"""Angular diameter distance in a curved FRW model (CFZ eqn. 1)"""
	dA = c*m.sinh(2.*r(z)) / ((1.+z) * H0 * m.sqrt(Ok))
	return dA

def H0_flat(z):
	"""Get the local Hubble parameter in the matched flat FRW universe"""
	return H0 * m.sqrt( 1. - (Ok*z/(1.+z)) )



def Sk(z):
	"""Shift parameter of a curved FRW model w.r.t. an EdS model"""
	return m.sqrt((1.+z) - z*Ok) * m.sinh(2.*r(z)) / ( 2.*m.sqrt(Ok)*(m.sqrt(1.+z) - 1.) )

def dHk(z):
	"""Hubble shift parameter of a curved model"""
	return H0_flat(z) / H0


def Sltb(z):
	"""Shift parameter of LTB model w.r.t. curved FRW"""
	return dA_LTB / (dA_k(z) * (1. + z) )


# Test these functions
print "-"*30

print "H0(EdS) =", H0_flat(zs)
print "dA(curved) =", dA_k(zs)
print "dA(curved,com) =", dA_k(zs)*(1.+zs)

print "-"*30

print "S(curved/EdS) =", Sk(zs)
print "S(LTB/curved) =", Sltb(zs)
print "dH(curved/EdS) =", dHk(zs)

print "-"*30

print "S(LTB/EdS) =", Sk(zs)*Sltb(zs)
