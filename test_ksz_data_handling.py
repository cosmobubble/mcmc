#!/usr/bin/python
"""Test the reading in of kSZ data"""

from read_reduced_ksz import read_data
import numpy as np
import pylab as P

z, vp, h = read_data()

for i in range(len(z)):
	print i, round(np.mean(z[i]), 2)
