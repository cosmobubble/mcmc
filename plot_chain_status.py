#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import sys

idd = int(sys.argv[1])
A = Ana.Analyse(idd)

avg, cov = A.covariance()
A.pretty_print_cov( avg, cov )
A.print_acceptance_rate()

# Plot series in Otot, Ok, h
P.subplot(331)
P.plot(A.column('Otot'), 'k-', label="Otot")
P.plot(A.column('Ok'), 'r-', label="Ok")
P.plot(A.column('h'), 'b-', label="h")
P.xlabel("Step")
#P.legend(loc='lower right')
P.title(str(sys.argv[1]))

# Plot series in Wk, Wtb
P.subplot(332)
P.plot(A.column('Wk'), 'b-', label="Wk")
P.plot(A.column('Wtb'), 'g-', label="Wtb")
P.xlabel("Step")
#P.legend()

# Plot series in Atb
P.subplot(333)
P.plot(A.column('Atb'), 'k-', label="Atb")
P.xlabel("Step")
#P.legend()


# Plot the chain's path in (Atb, h) space
P.subplot(334)
P.plot(A.column('Atb'), A.column('h'), 'b-')
P.xlabel("Atb")
P.ylabel("h")

# Plot the chain's path in (Atb, Ok) space
P.subplot(335)
P.plot(A.column('Atb'), A.column('Ok'), 'g-')
P.xlabel("Atb")
P.ylabel("Ok")

# Plot the chain's path in (Wk, Wtb) space
P.subplot(336)
P.plot(A.column('Wk'), A.column('Wtb'), 'r-')
P.xlabel("Wk")
P.ylabel("Wtb")


# Plot the ACFs for (h, Atb)
P.subplot(337)
P.plot(A.ACF('h'), 'k-')
P.plot(A.ACF('Atb'), 'r-')
P.plot(A.ACF('h', True), 'k-')
P.plot(A.ACF('Atb', True), 'r-')
P.xlabel("Lag")
P.ylabel("ACF")

# Plot the ACFs for (Wk, Wtb)
P.subplot(338)
P.plot(A.ACF('Wk'), 'b-')
P.plot(A.ACF('Wtb'), 'g-')
P.plot(A.ACF('Wk', True), 'b-')
P.plot(A.ACF('Wtb', True), 'g-')
P.xlabel("Lag")
P.ylabel("ACF")

# Plot the ACFs for (h, Atb)
P.subplot(339)
P.plot(A.ACF('Atb'), 'k-')
P.plot(A.ACF('Atb', True), 'k-')
P.xlabel("Lag")
P.ylabel("ACF")

P.show()
