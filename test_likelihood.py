#!/usr/bin/python
"""Cobbled-together code to display 2d likelihood plots for MCMC chains with larger number of parameters."""

import numpy as np
import pylab as P


def get_sig_levels(z):
    """Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
    linz = z.flatten()
    linz = np.sort(linz)[::-1]
    tot = sum(linz)
    acc = 0.0; i=-1; j=0
    lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
    slevels = []
    for item in linz:
	acc += item/tot
	i+=1
	if(acc >= lvls[j]):
            j+=1
            slevels.append(item)
    slevels.append(-1e-15) # Very small number at the bottom
    return slevels[::-1]


# Columns containing model input parameters
data = []
for i in range(8):
    fname = "mcmc_record-2223"+str(i+1)
    f = open(fname, 'r')
    for line in f.readlines():
        tmp = line.split("\t")
        data.append(tmp)

print len(data), data[0]
