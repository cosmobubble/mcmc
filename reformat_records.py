#!/usr/bin/python
"""Reprocess old mcmc_record files due to the insertion of an extra likelihood"""
"""(The new analysis tools now expect thios extra likelihood field to be in the record file)"""

import os

froot = "mcmc_record-"
idmin = 5000
idmax = 7999
nums = []

# Get filenames of records
for item in os.listdir('.'):
	if froot in item:
		this_id = int(item[len(froot):])
		if this_id > idmin and this_id < idmax:
			nums.append(this_id)

# Replace the records
for num in nums:
	fname = froot + str(num)
	f = open(fname, 'r')
	g = open(froot + '9' + str(num), 'a') # Prefix ID with 9 to identify it as reprocessed
	i = 0
	for line in f.readlines():
		tmp = line.split("\t")
		newline = tmp[:11] + [0.0] + tmp[11:]
		string = ""
		for item in newline:
			string += str(item)+"\t"
		g.write(string[:-1])
	f.close()
	g.close()
