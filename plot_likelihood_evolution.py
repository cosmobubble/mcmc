#!/usr/bin/python
"""Plot the evolution of likelihoods as the chain progresses"""

import hasty_analysis as Ana
from hasty_settings import *
import sys
import pylab as P
import numpy as np

idd = sys.argv[1] # get ID of chain to plot
A = Ana.Analyse(idd)

notfailed = [] # The likelihood of any step that didn't fail, even if it was rejected
accepted = [] # Likelihood only of accepted steps
sne = [] # Likelihood from accepted SNe
shift = [] # likelihood from accepted Shift

last_acc = 0.0
last_sne = 0.0
last_shift = 0.0
nf = 0.0

# Loop through all steps in chain
for step in A.steps:
	
	if step['rej'] == 'a':
		# Add latest accepted likelihoods to list
		notfailed.append( step['l'] )
		accepted.append( step['l'] )
		sne.append( step['lsne'] )
		shift.append( step['lshift'] )
		
		# Update last accepted likelihoods
		last_acc = step['l']
		last_sne = step['lsne']
		last_shift = step['lshift']
		
	elif step['rej'] == 'r':
		# Rejected, but update
		notfailed.append( step['l'] )
		nf = step['l']
		
		# Rejected, so keep constant
		accepted.append( last_acc )
		sne.append( last_sne )
		shift.append( last_shift )
		
	else:
		# Bad run, so keep all constant
		notfailed.append( nf )
		accepted.append( last_acc )
		sne.append( last_sne )
		shift.append( last_shift )
		
P.subplot(211)
P.plot(accepted, 'k-', label="Accepted")
P.plot(sne, 'r-', label="Accepted (SNe)")
P.plot(shift, 'b-', label="Accepted (Shift)")
P.yscale('log')
P.legend()

P.subplot(212)
P.plot(notfailed, 'k-', label="Accepted/Rejected")
P.plot(accepted, 'r-', label="Accepted")
P.yscale('log')
P.legend()

P.show()
