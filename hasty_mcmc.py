#!/usr/bin/python

import numpy.random as nran
import hasty_constraints as constraint
import subprocess, sys, os, signal, math, time
from hasty_settings import *
import hasty_observables as obs
import hasty_data as dat

class HastyMCMC(object):
	"""Control a Metropolis-Hastings MCMC"""
	
	def __init__(self, p, priors, cov, idd=0, num_runs=1000, constraint_mode=CONSTR_DEFAULT, temperature=1.0):
		"""Initialise the MCMC"""
		#self.free_params = {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5}
		self.free_params = {"Otot":0, "Ok":1, "Wk":2, "Atb":3, "Wtb":4, "h":5, 
							"offset_x":6, "offset_x2":7, "A2":8, "W2":9}
		self.dimensions = len(self.free_params)
		self.idd = idd # The ID of this MCMC run
		self.Cov = cov # Initial covariance matrix
		self.T = temperature # Temperature of the chain
		self.params = []
		self.result = [] # Values from the test (dH and S)
		self.rejected = [] # Whether the step was rejected or not
		self.lhood = [] # The likelihoods from the different observables
		self.returned_data = [] # Interesting data returned by each run
		self.times = [] # How long the run took
		self.CHAIN_FILE = "mcmc_record-" + str(idd)
		self.constraint_mode = constraint_mode # Constraints to apply when calculating the likelihood
		self.initial_params = p
		self.priors = priors # Prior bounds on function
		self.num_runs = num_runs
		nran.seed()
		
		# Preload data
		self.preloaded_sne_data = dat.sne_data_union2() # Load observational SNe data
		
		# Output MCMC info to log
		self.log("*** Applying constraints: " + str(constraint_mode))
		self.log("*** Temperature: " + str(self.T))
		self.log("*** Parameters: " + str(self.free_params))
		self.log("*** Priors: " + str(priors))
		self.log("*** Initial params: " + str(p))
		self.log("*** Covariance matrix: " + str(cov))
	
	def mcmc_control(self):
		"""Controls a Hastings-Metropolis MCMC run"""
		
		p = self.initial_params
		
		# Get a good initial step
		good_first_run = False
		while not good_first_run:
			success = self.run_model(p)
			
			# Check if the model ran successfully
			if success:
				try:
					l, data = self.likelihood()
				except:
					raise
					l = [BIGL]
					pass
			else:
				l = [BIGL]
			# Check if the model returned sensible likelihood
			if l[0] < BIGL:
				good_first_run = True # Accept initial step
			else:
				# Try again if not successful
				self.log("*** Warning: Attempt at getting a good first step failed (error code " + str(success) + "). Parameters: " + str(p))
				p = self.generate_trial_step(p) # Get new trial step
		
		# Save the first accepted step
		self.save_step(p, l, 'a', data)
		lold = l # Remember the old likelihood
		
		# Now, run the model
		acc = 0
		badrun = 0
		while acc < self.num_runs:
			
			oldtime = time.time()
			
			# Check if we've had load of bad runs
			if badrun > MAXBAD:
				self.log("*** CHAIN FAILED: Too many consecutive bad runs.")
				raise ValueError
			
			# Try a new step
			pnew = self.generate_trial_step(p)
			success = self.run_model(pnew)
			
			# Check if it was a success, otherwise fail gracefully
			if success:
				l, data = self.likelihood()
				
				# Check if it was too close to the LSS (ksz=4, local=5)
				if l[4] != 0.0:
					badrun += 1
					self.times.append(time.time() - oldtime)
					self.save_step(pnew, l, 'k', data)
				elif l[5] != 0.0:
					badrun += 1
					self.times.append(time.time() - oldtime)
					self.save_step(pnew, l, 'p', data)
				else:
					# It's not too close to the LSS
					# Decide whether to accept or reject
					# (Test if the likelihood ratio is above the threshold)
					alpha = nran.uniform()
					try:
						# Gives OverflowError if unchecked
						# (means it's big enough to automatically accept)
						L = math.exp((0.5/self.T) * (lold[0] - l[0]))
					except:
						L = +1.01 # P > alpha=U[0,1], so always accept
					if L > alpha:
						# Accept
						p = pnew
						self.times.append(time.time() - oldtime)
						self.save_step(p, l, 'a', data)
						lold = l # Store the likelihood of this, which is now the most recent accepted step
						badrun = 0
						acc += 1
					else:
						# Reject
						badrun += 1
						self.times.append(time.time() - oldtime)
						self.save_step(pnew, l, 'r', data)
					
			else:
				# If the code run was unsuccessful, make a note and try again
				l = [BIGL, BIGL, BIGL, BIGL, BIGL, BIGL] # Very low likelihood - bad value!
				self.times.append(time.time() - oldtime)
				self.save_step(pnew, l, 'b')
				badrun += 1
	
	"""
	def run_model(self, params):
		\"""Run model with the parameters in params\"""
		args = []
		args.append(EXEC_PATH)
		params = map(lambda x: str(round(x, 4)), params)
		args = args + params + [str(self.idd)]
		status = subprocess.call(args, cwd=WORKING_DIR)
		if status != 0:
			self.log("ERROR: Model run failed with status " + str(status))
			return False
		else:
			return True
	"""
	
	def run_model(self, params):
		"""Run model with the parameters in params"""
		args = []
		args.append(EXEC_PATH)
		params = map(lambda x: str(round(x, NUM_DP)), params)
		args = args + params + [str(self.idd)]
	
		# Run process and wait to see if it finishes in the allowed time
		proc = subprocess.Popen(args, cwd=WORKING_DIR, stderr=subprocess.PIPE)
		stderror = proc.communicate() # Standard output
		
		tstart = time.time()
		while proc.poll() is None:
			time.sleep(0.1)
			if time.time() - tstart > TIMEOUT:
				# Process has been running for too long
				self.log("ERROR: Model running for too long, killing it.")
				os.kill(proc.pid, signal.SIGKILL)
				os.waitpid(-1, os.WNOHANG)
				return False
		
		# Get the status of the process
		status = proc.poll()
		if status != 0:
			self.log("Model run failed with status " + str(status))
			self.log("Error report:" + str(stderror))
			return False
		else:
			return True
	
	
	def save_step(self, params, l, rejected, data = []):
		"""Store the results of the step"""
		
		self.params.append(params) # Add trial parameters to array
		self.lhood.append(l) # Add likelihood information
		self.rejected.append(rejected) # Add rejection information
		self.returned_data.append(data) # Add interesting data info
		
		# Store values in file
		line = ""
		for item in params:
			line += str(round(item, NUM_DP)) + "\t"
		for item in l:
			line += str(item) + "\t"
		line += str(rejected) + "\tx\t"
		for item in data:
			line += str(item) + "\t"
		
		# Diagnostic
		self.print_run_details(params, l, rejected)
		
		f = open(self.CHAIN_FILE, 'a')
		f.write(line + "\n")
		f.close()
	
	def likelihood(self):
		"""Calculate the likelihood and return interesting data which resulted from that process"""
		l = []
		data = []
		cm = self.constraint_mode
		
		# Get the chi-squared likelihoods from the models
		# Supernove
		if(cm['Sne']):
			l_sne, sn_offset = constraint.supernovae_union2(self.idd, self.preloaded_sne_data)
			l.append( l_sne )
		else:
			l.append(0.0)
			sn_offset = 0.0
		data.append(sn_offset)
		
		# Local Hubble rate
		if(cm['H0']):
			l.append( constraint.local_hubble_rate(self.idd) )
		else:
			l.append(0.0)
		
		# CMB shift parameters
		if(cm['Shift']):
			l.append( constraint.cmb_shift(self.idd) )
		else:
			l.append(0.0)
		
		# Get data about shift params
		try:
			sh = obs.obsParams(self.idd)
			dA = sh.dAdec*(1.+sh.zdec)
			H0 = 100.*sh.H0
			Hdec = 100.*sh.Hdec
		except:
			dA = 0.0
			H0 = 0.0
			Hdec = 0.0
			print "\tlikelihood() shift code failed - couldn't load model data"
			pass
		data.append(dA)
		data.append(H0)
		data.append(Hdec)
		
		# Prior-like consistency check on way the LSS is treated for outermost kSZ observer
		lkszprox, info_z = constraint.ksz_bang_time_proximity(self.idd, maxgrad=TBMAXGRAD)
		if info_z != 0.0:
			self.log("*** WARNING: Difference between z_LSS & z_DEC, d=" + str(info_z))
		if(cm['KLSS']):
			l.append(lkszprox)
			data.append(info_z)
		else:
			l.append(0.0)
			data.append(info_z)
		
		# Prior-like consistency check on way the LSS is treated for local observer
		llprox = constraint.local_bang_time_proximity(self.idd, maxgrad=TBMAXGRAD)
		if(cm['LLSS']):
			l.append(llprox)
		else:
			l.append(0.0)
		
		# kSZ data
		l_ksz = constraint.ksz(self.idd)
		if(cm['kSZ']):
			l.append(l_ksz)
		else:
			l.append(0.0)
		
		# Add summed likelihood to list and return
		return [sum(l)] + l, data
	
	def check_priors(self, p):
		"""Check whether a set of parameters falls inside the allowed region"""
		status = True
		
		for i in range(self.dimensions):
			xmin = self.priors[i][0]
			xmax = self.priors[i][1]
			x = p[i]
			if(x < xmin or x > xmax):
				return False
			
		return status
	
	def generate_trial_step(self, p):
		"""Generate a new step"""
		# New step, centred around the current one, with the current covariance
		new_params = nran.multivariate_normal(p, self.Cov)
		
		# Check step validity
		if self.check_priors(new_params):
			# The new parameters are OK, return them
			return new_params
		else:
			# New params fall outside the prior bounds, try again
			return self.generate_trial_step(p)
	
	
	def calculate_cov(self):
		"""Calculate the covariance matrix (so far) for the MCMC run"""
		# First, calculate the averages
		avg = []
		n = len(self.params)
		for j in range(self.dimensions):
			avg.append(0.0)
			for i in range( len(self.params) ):
				avg[j] += self.params[i][j]
			avg[j] /= float(n)
		
		cov = []
		for i in range(self.dimensions):
			for j in range(self.dimensions):
				pass
				
		# Now, calculate the variances		
		for i in range(self.dimensions):
			for j in range(self.dimensions):
				# Go through each datapoint
				for k in range( len(self.params) ):
					cov[i][j] += (avg[i] - self.params[k][i]) \
							  *  (avg[j] - self.params[k][j])
				cov[i][j] /= float(n)
		return avg, cov
	
	def log(self, msg):
		"""Output a message to the run log"""
		print msg
		fname = "log-" + str(self.idd)
		f = open(fname, 'a')
		f.write(str(msg) + "\n")
		f.close()
	
	def print_run_details(self, params, likelihoods, rej):
		"""Print diagnostic details of a run"""
		msg = ""
		
		# Print basic run details
		msg += "(" + str(len(self.times)) + ") "
		if(rej=='a'):
			msg += "ACCEPTED "
		elif(rej == 'r'):
			msg += "REJECTED "
		elif(rej == 'b'):
			msg += "RUN FAILED "
		elif(rej == 'k'):
			msg += "TOO CLOSE TO KSZ LSS "
		elif(rej == 'p'):
			msg += "TOO CLOSE TO LOCAL LSS "
		if len(self.times)>0:
			msg += "\t" + str(round(self.times[len(self.times)-1], 2)) + " sec"
		else:
			msg += "n/a"
		msg += "\n"
		
		# Print parameters
		for pkey in self.free_params.keys():
			i = self.free_params[pkey]
			msg += (pkey + ": " + str(params[i]) + ",  ")
		msg += "\n"
		
		# Print likelihoods
		names = ["Total", "SNe", "H0", "Shift", "kSZ_LSS_prox", "local_LSS_prox", "kSZ"]
		for i in range(len(likelihoods)):
			msg += (names[i] + ": " + str(likelihoods[i]) + ",  ")
		msg += "\n"
		msg += "-"*50
		
		# Log output
		self.log(msg)

