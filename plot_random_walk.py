#!/usr/bin/python

import hasty_analysis as Ana
import pylab as P
import sys

idd = int(sys.argv[1])
A = Ana.Analyse()
cols = ["k-", "r-", "b-", "g-", "y-", 'c-']

# Draw a matrix of possible random walk combinations

vn = A.data_varnames
for i in range(len(vn)):
	for j in range(len(vn)):
		if(j>i):
			P.subplot(len(vn)-1, len(vn)-1, 1+(i)+(len(vn)-1)*(j-1))
			P.plot(A.column(vn[i]), A.column(vn[j]), cols[i])
			if(i==0):
				P.ylabel(vn[j])
			if(j==i+1):
				P.title(vn[i])

P.show()
