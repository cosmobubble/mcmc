#!/usr/bin/python
"""Get chi-squared for Lambda CDM model"""
# Numbers from http://lambda.gsfc.nasa.gov/product/map/dr4/params/lcdm_sz_lens_wmap7_h0.cfm

import lcdm_parameters
import sys, time, os, subprocess
import hasty_constraints as HC
import hasty_data as HD
import hasty_observables as HO
import get_bubble_data
from hasty_settings import *
import numpy as np
import scipy.optimize as opt

###########################################
def sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov):
	"""Get the SNe chi-squared for a given offset a"""
	# dm_milne = 5log10(C*[z + 0.5z^2]) - 5 log10(H0)
	v = ddm_data - ddm_model - ddm_milne - a
	mat = np.dot(icov, np.transpose(v))
	x2 = np.dot(v, mat)
	return float(x2)

def sn_best_fit_offset(ddm_data, ddm_model, ddm_milne, icov):
	"""Find the H0 which minimizes the chi-squared"""
	ddm_data = np.matrix(ddm_data)
	ddm_model = np.matrix(ddm_model)
	ddm_milne = np.matrix(ddm_milne)
	# Find deltaDM offset which minimises the chi-squared (initial guess: 1.0)
	try:
		offset = opt.leastsq(sn_chisq, 1.0, args=(ddm_data, ddm_model, ddm_milne, icov))
		a = float(offset[0]) # Extract offset from array
		x2 = float(sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov)) # Chi-squared for best-fit value
	except:
		raise
		# Fail gracefully, by rejecting this run
		print "ERROR: Failed to find minimum chi-squared for supernova data"
		a = 1e5
		x2 = 1e8
	return x2, a

###########################################

def ksz_chisq(vp_model, vp_obs, err_obs):
	"""Get the chi-squared between the model and the kSZ data"""
	x2 = 0.0
	j = 0
	for i in range(len(vp_obs)):
		if len(vp_model) < i+1:
			vpm = 3e5 # Maximal inhomogeneity
		else:
			vpm = vp_model[i]
		j += 1
		x2 += (vp_obs[i] - vpm)**2. / err_obs[i]**2.0
	return x2

def get_ksz_obs_data():
	"""Get observed kSZ velocities, the ones quoted in GBH"""
	d = np.genfromtxt("../bubble/obsdata/GBH_kSZ_data.dat", unpack=True)
	z = d[1]
	vp = d[2]
	err = d[3]
	return z, vp, err

###########################################

def hubble_chisq(H0):
	"""Find the chi-squared for the H0 data"""
	hub = HD.hubble_data()[0] # Load H0 measurement data (only one datapoint)
	x2_hub = (hub['H0'] - H0)**2.0 / (hub['errH0']**2.0)
	return x2_hub

###########################################

def cmb_chisq(Hdec, dAdec, zdec):
	"""Calculate chi-squared for the shift parameter CMB data from WMAP 7 + my CosmoMC run"""
	C = 3e5
	shdat = HD.shift_params() # Load Shift parameter *data*
	
	# Calculate shift params from the calc. model data
	S = Hdec * dAdec / (2.*C * ( (1.+zdec)**0.5 - 1.) )
	H0eds = (1. + zdec)**(-1.5) * Hdec
	cov = shdat['cov']
	
	# Calculate chi-squared (log likelihood) using shift param covariance matrix
	v = [ S - shdat['S'], H0eds - shdat['H0eds'] ] # Vector of deviations
	x2_cmb = np.dot(np.transpose(v), np.dot(np.linalg.inv(cov), v))
	return x2_cmb

###########################################

# LCDM parameters taken from WMAP best fit
zstar = 1090.45
dA = 14156 / (1.+zstar) # dA(z*) Mpc
H0 = 72.1
Om = 0.254
Ol = 0.746
L = lcdm_parameters.Model(H0, Om, Ol) # Use this to get deltaDM only!
Hdec = H0 * np.sqrt(Om*(1.+zstar)**3. + Ol) # Just use the no-radiation approximation (radiation changes H* a lot! But MCMC constraints are derived for "no radiation", EdS models)

x2 = []

print "-"*40

################################################################################
# Union2 supernova chi-squared

sne_data = HD.sne_data_union2() # Load observational SNe data
dm_lcdm = map(lambda zz: L.deltaDM(zz), sne_data['z']) # Calculate LCDM dDMs
x2_sne, a = sn_best_fit_offset(sne_data['dm'], dm_lcdm, sne_data['dm_milne'], sne_data['icov'])
x2.append( x2_sne )
print "SNe:", round(x2_sne, 3), "dof=", 557

################################################################################
# WMAP 7 CMB chi-squared

x2_cmb = cmb_chisq( Hdec, dA, zstar )
x2.append( x2_cmb )
print "CMB:", round(x2_cmb, 3), "dof=", 2

################################################################################
# HST H0 chi-squared

x2_h0 = hubble_chisq(H0)
x2.append( x2_h0 )
print "H0:", round(x2_h0, 3), "dof=", 1

################################################################################
# kSZ chi-squared
# LCDM has beta=0
oz, ovp, oerr = get_ksz_obs_data()
vp_lcdm = map(lambda x: 0.0, oz)
x2_ksz = ksz_chisq(vp_lcdm, ovp, oerr)
x2.append( x2_ksz )
print "kSZ:", round(x2_ksz, 3), "dof=", 9

# Total chi-squared
print "-"*40
print "Sum =", round(sum(x2), 3)
print "x2/dof =", round(sum(x2) / (557+2+1+9), 3)
print "-"*40
