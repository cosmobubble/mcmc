#!/usr/bin/python

# Import relevant modules
import pymc
import numpy as np
import numpy.random as nran
import math
import lcdm_parameters as M


################################################################################
# Generate data points of H(z) which are consistent with the model
# H0 = 72, Om = 0.3, OL = 0.7
nran.seed()
LCDM = M.Model(72, 0.3)
oz = []
for i in range(100):
	oz.append( nran.uniform() ) # Redshifts between 0 and 1
oH = [] # "Observed" H(z)
sH = [] # "Observed" error on H(z) values

for z in oz:
	hz = LCDM.H(z)*0.3
	h = nran.normal(hz, hz*0.01) # 1% error on H(z) measurements
	sh = nran.normal(hz*0.01, hz*0.001) # Simulate "quoted" error, not exactly 1%
	oH.append(h)
	sH.append(sh)

print "\tGenerated H(z) sample for reference model"


# Priors on unknown parameters
alpha = pymc.Normal('alpha',mu=70.,tau=4.)
beta = pymc.Normal('beta',mu=0.25,tau=.05)

def likelihood(mod):
	"""Calculate the likelihood"""
	l = 0.0
	for i in range(len(oz)):
		l += ( mod.H(oz[i])*mod.Om - oH[i] )**2.0 / sH[i]**2.0
	return -l / float( len(oz) )

# Arbitrary deterministic function of parameters
@pymc.deterministic
def theta(a=alpha, b=beta):
	mod = M.Model(a, b)
    return math.exp( likelihood(mod) )
    #return pymc.invlogit(a+b*x)

# Binomial likelihood for data
d = math.exp(likelihood(mod))
d = pymc.Binomial('d', n=n, p=theta, value=np.array([0.,1.,3.,5.]), observed=True)
