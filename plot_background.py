#!/usr/bin/python

from pylab import *
import sys
import hasty_constraints as HC
import hasty_data as HD
import scipy.interpolate as interp

idd = str(sys.argv[1])

# Lists of calculated variables
r = [] # Comoving radial distance
density = [] # Physical density
# Scale factors
a1 = []
a1dot = []
a2 = []
a2dot = []
# Hubble rates
Ht = []
Hr = []
k = []
pk = []
pm = []
tb = []
tbprime = []

gr = []
gt = []
gz = []
gdm = []
gzeds = []
gdmeds = []
gdl = []
gdleds = []
gdmmilne = []

rr = []
zz = []
dz = []
rend = []
dens = []

Rrr = []
Rzz = []
Rdz = []

# Get data
f = open("../bubble/data/background-"+idd, 'r')

for line in f.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		r.append(float(tmp[0]))
		density.append(float(tmp[1]))
		a1.append(float(tmp[2]))
		a1dot.append(float(tmp[3]))
		a2.append(float(tmp[4]))
		a2dot.append(float(tmp[5]))
		Ht.append(float(tmp[6]))
		Hr.append(float(tmp[7]))
		k.append(float(tmp[8]))
		pk.append(float(tmp[9]))
		pm.append(float(tmp[10]))
		tb.append(float(tmp[11]))
		tbprime.append(float(tmp[13]))
f.close()

# Get data
g = open("../bubble/data/geodesic-"+idd, 'r')

for line in g.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 8:
		gr.append(float(tmp[0]))
		gt.append(float(tmp[1]))
		gz.append(float(tmp[2]))
		gdm.append(float(tmp[3]))
		gzeds.append(float(tmp[4]))
		gdmeds.append(float(tmp[5]))
		gdl.append(float(tmp[6]))
		gdleds.append(float(tmp[7]))
		gdmmilne.append(float(tmp[8]))
g.close()

# Get vp(z) data
h = open("../bubble/data/velocity-"+idd, 'r')

for line in h.readlines():
	tmp = line.split("\t")
	if len(tmp) >= 3:
		rr.append(float(tmp[0]))
		zz.append(float(tmp[1]))
		dz.append(float(tmp[2]))
		rend.append(float(tmp[5]))
		dens.append(float(tmp[8]))
h.close()

# Model info
h = open("../bubble/data/modelinfo-"+idd, 'r')
modname = h.readlines()[0][:-1]

# Measured SNe data
sne = HD.sne_data()
sneZ = map(lambda x: x['z'], sne)
sneDM = map(lambda x: x['dm'], sne)
sneERR = map(lambda x: x['err'], sne)

################################################################################
# Likelihoods

print "\n", modname, "\n"

Lsne = HC.supernovae(idd)
Lcmb = HC.cmb_shift(idd)
Lh0 = HC.local_hubble_rate(idd)
Lksz = HC.ksz_bang_time_proximity(idd, maxgrad=1e-10)[0]
Ltb = HC.local_bang_time_proximity(idd, maxgrad=1e-10)
L = Lsne + Lcmb + Lh0 + Lksz + Ltb

Lstring = ""
Lstring += "Sne:    " + str(round(Lsne, 2)) + "\n"
Lstring += "CMB:    " + str(round(Lcmb, 2)) + "\n"
Lstring += "H0:     " + str(round(Lh0, 2)) + "\n"
Lstring += "kSZ:    " + str(round(Lksz, 2)) + "\n"
Lstring += "tB:     " + str(round(Ltb, 2)) + "\n"
Lstring += "TOTAL:  " + str(round(L, 2))

################################################################################
# Interpolate redshifts and r's
# Use gr, gz to get interpolator
def zinterp(rr):
	# Find the r array value that is closest to rr
	ii = 0
	minval = 1e5 # Set to large number initially
	for i in range(len(gr)):
		diff = abs(gr[i] - rr)
		if diff < minval:
			minval = diff
			ii = i
	# Find a slice of r array values near to the r value (5 values)
	if(ii <= 2):
		# Near the start of the array
		imin = 0
		imax = 4
	elif(ii >= len(gr) - 3):
		# Near the end of the array
		imin = len(gr) - 5
		imax = len(gr) - 1
	else:
		# Somewhere in the middle
		imin = ii - 2
		imax = ii + 2
		
		# Build a spline using this slice of the arrays
		sp = interp.UnivariateSpline(gr[imin:imax], gz[imin:imax], k=3)
		return sp(rr)

r_z = map(lambda x: zinterp(x), r) # Interpolate for whole array

################################################################################
# Plots
ZMAX = 3.
figtext(0.75, 0.15, Lstring)

# Density profile
subplot(231)
plot(r_z, density, 'r-')
grid(True)
title('$\\rho(r)$')
xlim(0.0, ZMAX)

# Bang time profile
subplot(232)
plot(r_z, tb, 'r-')
grid(True)
title('tB(r)')
xlim(0.0, ZMAX)

# Curvature profile
subplot(233)
plot(r_z, k, 'b-')
grid(True)
title('k(r)')
xlim(0.0, ZMAX)

# Expansion rates
subplot(234)
plot(r_z, Ht, 'b-', r_z, Hr, 'r-')
grid(True)
title('Hubble Rates')
xlim(0.0, ZMAX)

# Supernovae
subplot(235)
errorbar(sneZ, sneDM, yerr=sneERR, fmt="r.")
plot(gz[1:], gdm[1:], 'k-')
grid(True)
title('deltaDM(z)')
xlabel('z')

show()
