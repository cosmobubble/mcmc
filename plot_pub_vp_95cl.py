#!/usr/bin/python
"""Graph for publication. beta(z) vs. z for z<0.3, with 1sigma and 2sigma CL bounds.
   The data are from runs 1100(8), which have Atb!=0 and the full CMB+H0+Sne constraints
   (although the Sne are pre-Union2, using Ned Wright's binned distance moduli)
   Note that samples with beta > 200x10^3 km/s have been trimmed out to get the bounds"""

import numpy as np
import pylab as P
import scipy.stats as stats
from read_reduced_ksz import read_data


def get_ksz_obs_data():
	"""Get observed kSZ velocities, the ones quoted in GBH"""
	d = np.genfromtxt("../bubble/obsdata/GBH_kSZ_data.dat", unpack=True)
	z = d[1]
	vp = d[2]*1e-3 # Units are now (10^3 km/s)
	err = d[3]*1e-3
	return z, vp, err

def make_polygon(z, p, start=0):
	"""Make a polygon to represent the n-sigma bounds"""
	"""Here, 'start' is which bounds to pick out"""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(z)):
		poly.append( [z[i], p[start][i]] )
	# Now go in reverse for lower bounds
	for i in range(len(z))[::-1]:
		poly.append( [z[i], p[start+1][i]] )
	return poly

# Get kSZ vp(z) data
o_z, o_vp, o_err = get_ksz_obs_data()

# Get vp(z) data for MCMC
z, vp, h = read_data()

# Filter-out high vp(z) values (above 200,000 km/s) [N.B. CHANGED TO 300,000 km/s]
MAX_BETA = 200. #301. # 200.
vp_low = []
z_low = []
for i in range(len(vp)):
	vp_low.append([])
	z_low.append([])
	for j in range(len(vp[i])):
		if vp[i][j] < MAX_BETA:
			vp_low[i].append(vp[i][j]/300.) # Convert to units of dT/T
			z_low[i].append(z[i][j])

# Only use the lower redshifts, not much left if you only allow vp<200,000 at higher z!
z = z_low[:6]
vp = vp_low

# Upper and lower percentiles
#pctiles = [68.27, 95.45, 99.73]
#pct = [ [], [], [], [], [], [] ]
# The percentiles to plot (1sigma, 2sigma)
pctiles = [68.27, 95.45]
pct = [ [], [], [], [] ]
cols = ['r-', 'b-', 'g-']
median = []
zz = []

# Get percentiles and medians for each redshift
for i in range(len(z)):
	median.append( np.median(vp[i]) )
	zz.append(z[i][0])
	for j in range(len(pctiles)):
		# Calculate the percentiles (confidence intervals)
		plus = 100. - 0.5*(100. - pctiles[j])
		minus = 0.5*(100. - pctiles[j])
		# Get the value of vp at these percentiles
		pct[2*j].append( stats.scoreatpercentile(vp[i], plus) )
		pct[2*j+1].append( stats.scoreatpercentile(vp[i], minus) )

# Make the plot
P.subplot(121)

# Add 1s/2s bounds by drawing grey polygons
P.gca().add_patch(P.Polygon(make_polygon(zz, pct, start=2), closed=True, fill=True, color="#B9B9B9"))
P.gca().add_patch(P.Polygon(make_polygon(zz, pct, start=0), closed=True, fill=True, color="#e9e9e9"))

P.plot(zz, median, 'k-', lw=1.5)

# Set font size
fontsize = 20.
ax = P.gca()
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)

"""
P.errorbar(o_z, o_vp, yerr=o_err, fmt=',', color='k') # Error bars from real kSZ data
# Plot lines for n-sigma bounds
for i in range(len(pctiles)):
	P.plot(zz, pct[2*i], cols[i], zz, pct[2*i], cols[i][0]+"+")
	P.plot(zz, pct[2*i + 1], cols[i], zz, pct[2*i + 1], cols[i][0]+"+")
"""

# Get rid of the first tick
ax.yaxis.get_major_ticks()[0].label1.set_visible(False)

# Axis labels
#P.ylabel(r"$\beta$ $(z)$ $[10^3 kms^{-1}]$", fontdict={'fontsize':'x-large', 'weight':'heavy'})
P.ylabel(r"$\Delta T / T$", fontdict={'fontsize':'24', 'weight':'heavy'})
#P.xlabel("$z$", fontdict={'fontsize':'xx-large', 'weight':'heavy'})
P.xlabel("z", fontdict={'fontsize':'24'})


# Add second axis which is in 10^3 km/sec
ax2 = P.twinx()
dt_min, dt_max = ax.get_ylim() # Get axis bounds from other axis
P.ylim((dt_min * 3e2, dt_max * 3e2)) # Set limits for this axis in 10^3 km/sec
P.ylabel(r"$\beta(z) [10^3 kms^{-1}]$", fontdict={'fontsize':'24', 'weight':'heavy'})

# Set font size
fontsize = 20.
for tick in ax2.yaxis.get_major_ticks():
	# N.B. 'label2' is the right-aligned label.
	# 'label1' is the left-aligned one (used above).
	tick.label2.set_fontsize(fontsize)

# Get rid of the first tick for this axis too
ax2.yaxis.get_major_ticks()[0].label2.set_visible(False)

P.show()
