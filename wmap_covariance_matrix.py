#!/usr/bin/python
"""Take a WMAP MCMC chain and output a covariance matrix for given parameters"""
# http://lambda.gsfc.nasa.gov/product/map/dr4/params/lcdm_sz_lens_wmap7.cfm

import pylab as P
import numpy as np
#PATH = "chain1/"
PATH = "chain2/"
variables = ["omegam", "omegal", "H0", "zeq", "zstar", "dastar"]

def get_weights():
	"""Get the weights used by the WMAP MCMC's importance sampling method"""
	# See http://cosmologist.info/cosmomc/chains.html
	f = open(PATH+"weight", 'r')
	dtmp = []
	for line in f.readlines():
		for i in range(10):
			line = line.replace("  ", " ")
		tmp = line.split(" ")
		if len(tmp)>2:
			dtmp.append(float(tmp[2]))
	return dtmp

def cov():
	"""Return the mean vector and covariance matrix of a set of WMAP data"""
	
	w = get_weights() # Need to weight the data - to do with importance sampling
	
	# Get the data from the MCMC files
	data = []
	j = -1
	for var in variables:
		f = open(PATH+var, 'r')
		dtmp = []
		for line in f.readlines():
			for i in range(10):
				line = line.replace("  ", " ")
			tmp = line.split(" ")
			if len(tmp)>2:
				dtmp.append(float(tmp[2]))
		data.append(dtmp)

	# Calculate the covariances in a basic (unweighted) way
	#cov = np.cov(data)
	
	# Calculate the means
	mean = []
	for i in range(len(variables)):
		u = np.average(data[i], weights=w)
		mean.append(u)
	
	# Manually calculate the weighted covariance
	cov = []
	sw = 0.0
	for ww in w:
		sw += ww
	for i in range(len(data)):
		cov.append([])
		for j in range(len(data)):
			c = 0.0
			for k in range(len(data[i])):
				c += w[k]*(data[i][k] - mean[i])*(data[j][k] - mean[j])
			cov[i].append(c/sw)
	
	return mean, cov

def get_data(var):
	"""Get the data for some variable var"""
	# Get the data from the MCMC files
	f = open(PATH+variables[var], 'r')
	dtmp = []
	for line in f.readlines():
		for i in range(10):
			line = line.replace("  ", " ")
		tmp = line.split(" ")
		if len(tmp)>2:
			dtmp.append(float(tmp[2]))
	return dtmp
