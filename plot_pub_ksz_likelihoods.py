#!/usr/bin/python

from pylab import *
import sys, time, os, subprocess
import hasty_constraints as HC
import hasty_data as HD
import hasty_observables as HO
import get_bubble_data
from hasty_settings import *
import numpy as np
import scipy.optimize as opt

tstart = time.time()
ZMAX = 6.
idd = 199

###########################################
def sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov):
	"""Get the SNe chi-squared for a given offset a"""
	# dm_milne = 5log10(C*[z + 0.5z^2]) - 5 log10(H0)
	v = ddm_data - ddm_model - ddm_milne - a
	mat = np.dot(icov, np.transpose(v))
	x2 = np.dot(v, mat)
	return float(x2)

def sn_best_fit_offset(ddm_data, ddm_model, ddm_milne, icov):
	"""Find the H0 which minimizes the chi-squared"""
	ddm_data = np.matrix(ddm_data)
	ddm_model = np.transpose( np.matrix(ddm_model) )
	ddm_milne = np.matrix(ddm_milne)
	# Find deltaDM offset which minimises the chi-squared (initial guess: 1.0)
	try:
		offset = opt.leastsq(sn_chisq, 1.0, args=(ddm_data, ddm_model, ddm_milne, icov))
		a = float(offset[0]) # Extract offset from array
		x2 = float(sn_chisq(a, ddm_data, ddm_model, ddm_milne, icov)) # Chi-squared for best-fit value
	except:
		raise
		# Fail gracefully, by rejecting this run
		print "ERROR: Failed to find minimum chi-squared for supernova data"
		a = 1e5
		x2 = 1e8
	return x2, a

def supernovae_union2(idd, sne_data):
	"""Get the chi-squared for UNION2 SNe data, from chain with id=idd"""
	try:
		ddm = HO.deltaDM(idd)
	except:
		print "\tsupernovae() failed - couldn't load model data"
		return 1e8
	
	dm_model = map(lambda zz: ddm.DM(zz), sne_data['z']) # Model deltaDM for each SN z value
	l_sne, a = sn_best_fit_offset(sne_data['dm'], dm_model, sne_data['dm_milne'], sne_data['icov'])
	return l_sne, a

###########################################



def ksz_chisq(vp_model, vp_obs, err_obs):
	"""Get the chi-squared between the model and the kSZ data"""
	#print len(vp_model), vp_model
	#print len(vp_obs), vp_obs
	x2 = 0.0
	for i in range(len(vp_obs)):
	#for i in range(5):
		if len(vp_model) < i+1:
			vpm = 3e5 # Maximal inhomogeneity
		else:
			vpm = vp_model[i]
		x2 += (vp_obs[i] - vpm)**2. / err_obs[i]**2.0
	return x2

def get_ksz_obs_data():
	"""Get observed kSZ velocities, the ones quoted in GBH"""
	d = np.genfromtxt("../bubble/obsdata/GBH_kSZ_data.dat", unpack=True)
	z = d[1]
	vp = d[2]
	err = d[3]
	return z, vp, err

def run_model(idd, params):
	"""Run model with the parameters in params"""
	args = []
	args.append(EXEC_PATH)
	params = map(lambda x: str(round(x, 4)), params)
	
	# Add extra two arguments [0,0] so it does the full Bubble run
	args = args + params + [str(idd)] + ['0', '0']
	for prm in params:
		print prm,
	print ""
	
	# Run process and wait to see if it finishes in the allowed time
	proc = subprocess.Popen(args, cwd=WORKING_DIR, stderr=subprocess.PIPE)
	stderror = proc.communicate() # Standard output
	
	tstart = time.time()
	while proc.poll() is None:
		time.sleep(0.1)
		if time.time() - tstart > TIMEOUT:
			# Process has been running for too long
			print "FAIL"
			print "\tERROR: Model running for too long, killing it."
			os.kill(proc.pid, signal.SIGKILL)
			os.waitpid(-1, os.WNOHANG)
			return False
	
	# Get the status of the process
	status = proc.poll()
	if status != 0:
		#print "FAIL"
		#print "\tModel run failed with status", status
		print "\tError report:", stderror
		return False
	else:
		print "AOK"
		return True

def get_ksz_data(idd):
	"""Get vp(z) data"""
	h = open("../bubble/data/velocity-"+str(idd), 'r')
	rr = []
	zz = []
	dz = []
	rend = []
	dens = []
	for line in h.readlines():
		tmp = line.split("\t")
		if len(tmp) >= 3:
			rr.append(float(tmp[0]))
			zz.append(float(tmp[1]))
			dz.append(float(tmp[2]))
	h.close()
	
	d = { 'z': zz, 'vp':dz }
	return d

################################################################################
# Scan desired parameter space

# Get observational data
oz, ovp, oerr = get_ksz_obs_data()
#sne = HD.sne_data()
preloaded_sne_data = HD.sne_data_union2() # Load observational SNe data

# DEFAULT PARAMS
#Wtb = 4000.0
Wtb = 2000.0
pp = []

atb = np.arange(-2500., 2500., 100.)
print "Calculating", len(atb), "samples"
for this_atb in atb:
	pp.append( [0.75, 0.75, 2000., this_atb, 1800., 0.5] )

"""
pp.append( [0.85, 0.85, 1800., -200.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., -150.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., -100.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., -50.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., -25.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 0.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 25.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 50.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 100.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 150.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 200.0, Wtb, 0.7] )
pp.append( [0.85, 0.85, 1800., 250.0, Wtb, 0.7] )
"""

vp = []
z = []
ddm = []
sn_z = []
sn_a = []
Ab = []
lbls = []
x2_sne = []
x2_ksz = []
x2 = []
params = []

for i in range(len(pp)):
	p = pp[i]
	lbl = str(p[0]) + ", " + str(p[3]) + ", " + str(p[4])
	
	if not run_model(idd, p):
		print "MODEL RUN FAILED"
		try:
			d = get_ksz_data(idd)
			d2 = get_bubble_data.fetch(idd)
			lsne, a = supernovae_union2(idd, preloaded_sne_data)
			x2_sne.append(lsne)
			vp.append(d['vp'])
			z.append(d['z'])
			ddm.append(d2['gdm'])
			sn_z.append(d2['gz'])
			sn_a.append(a)
			lbls.append(lbl)
			Ab.append(p[3])
			params.append(p)
		except:
			print "\t*** Couldn't get any data."
			pass
	else:
		d = get_ksz_data(idd)
		d2 = get_bubble_data.fetch(idd)
		lsne, a = supernovae_union2(idd, preloaded_sne_data)
		x2_sne.append(lsne)
		vp.append(d['vp'])
		z.append(d['z'])
		ddm.append(d2['gdm'])
		sn_z.append(d2['gz'])
		sn_a.append(a)
		lbls.append(lbl)
		Ab.append(p[3])
		params.append(p)

for v in vp:
	x2_ksz.append(ksz_chisq(v, ovp, oerr))


# vp(z) profile vs. z (function of A_tb)
subplot(311)
for i in range(len(lbls)):
	plot( z[i], vp[i], label=lbls[i], marker="+" )
errorbar(oz, ovp, yerr=oerr, fmt=',', color='k')
grid(True)
xlabel('z')
ylabel('vp(z) [km/s]')
legend(prop={'size':'x-small'})

# Plot supernova data
subplot(312)
sd = preloaded_sne_data
errorbar(sd['z'], sd['dm'] - sd['dm_milne'], yerr=sd['err'], ls='None', marker=",", color='gray')
for i in range(len(lbls)):
	xx = []
	for k in range(len(ddm[i])):
		xx.append( ddm[i][k] + sn_a[i] )
	plot(sn_z[i], xx)
xlabel('z')
ylabel('ddM(z)')
xlim((0.0, 2.0))

for i in range(len(lbls)):
	x2.append(x2_sne[i] + x2_ksz[i])

# Chi-squared vs Atb
subplot(313)
plot(Ab, x2_sne, 'r+', Ab, x2_sne, 'r-')
plot(Ab, x2_ksz, 'b+', Ab, x2_ksz, 'b-')
plot(Ab, x2, 'k+', Ab, x2, 'k-')
xlabel('$A_{B}$ [Myr]')
ylabel('$\chi^2$')

print "\nCHI-SQ SUMMARY:"
for i in range(len(lbls)):
	print lbls[i], "\t", round(x2_sne[i],2), round(x2_ksz[i],2), round(x2_sne[i]+x2_ksz[i],2), round(sn_a[i], 2)
print "\n"

print "Finished parameter scan in", round((time.time() - tstart)/60., 1), "min."
show()

# Output results of calculations
f = open("ksz-fit-data-wtb1800c", 'w')
for i in range(len(lbls)):
	f.write("Run " + str(i) + ", Atb=" + str(Ab[i]) )
	f.write(", ksz-chi-sq=" + str(ksz_chisq(vp[i], ovp, oerr)) )
	f.write(", sne-chi-sq=" + str(x2_sne[i]) + "\n")
	f.write(str(params[i]) + "\n")
	for j in range(len(vp[i])):
		f.write( str(vp[i][j]) + "\t" + str(z[i][j]) + "\n" )
f.close()
