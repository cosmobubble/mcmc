#!/usr/bin/python
"""Plot shift parameter dependences"""

import pylab as P
import shift_params as S
import numpy as np

dA = 14100.55
Hstar = 1530518.
zstar = 1090.851
zeq = 3238.61
H0 = 70.

xx = np.arange(0.1, 1000., 0.1)
xx2 = np.arange(0.1, 10., 0.01)
ss = map( lambda x: S.Shift(dA, Hstar, zstar, x*zeq), xx )
dh = map( lambda x: S.dH(H0, Hstar, zstar, x*zeq), xx )
ss2 = map( lambda x: S.Shift(dA*x, Hstar, zstar, zeq), xx2 )
dh2 = map( lambda x: S.dH(H0*x, Hstar, zstar, zeq), xx2 )

P.subplot(221)
P.plot(xx, ss)
P.xscale('log')
P.xlabel('zeq / zeq(LCDM)')
P.ylabel('Shift parameter S')
P.grid(True)

P.subplot(222)
P.plot(xx, dh, 'r-')
P.xscale('log')
P.xlabel('zeq / zeq(LCDM)')
P.ylabel('Shift parameter dH')
P.grid(True)

P.subplot(223)
P.plot(xx2, ss2)
P.xscale('log')
P.xlabel('dA / dA(LCDM)')
P.ylabel('Shift parameter S')
P.grid(True)

P.subplot(224)
P.plot(xx2, dh2, 'r-')
P.xscale('log')
P.xlabel('H0 / H0(LCDM)')
P.ylabel('Shift parameter dH')
P.grid(True)

P.show()
