#!/usr/bin/python
"""Read-in the reduced ksz file"""

import numpy as np
import pylab as P

def read_data(fname="ksz-data-1100"):
    """Read-in the reduced kSZ data from the filename given"""
    # Definition of z values
    zvals = np.array([0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 1.0])
	
    # Read-in the data
    f = open(fname, 'r')
    dat = []
    for line in f.readlines():
        tmp = line.split("\t")
        if len(tmp) > 3:
            for i in range(len(tmp)):
                tmp[i] = float(tmp[i])
            dat.append(tmp)
	
    # Reduce into a single-column form; each z sample takes up one column
    vp = []
    z = []
    h = []
    for i in range(10):
        vp.append([])
        z.append([])
        h.append([])

    # For each record, split-out data
    for i in range(len(dat)):
        nsamples = int(dat[i][1])
        # Separate interleaved z/vp data
        for j in range(nsamples):
            x = 2*(j+1)
            cur_z = dat[i][x]
            # Get the correct index for this z value
            idx = (np.abs(cur_z - zvals)).argmin()
            
            z[idx].append( cur_z )
            vp[idx].append( dat[i][x+1] )
            h[idx].append( dat[i][0] )

    return z, vp, h
