#!/usr/bin/python

import pylab as P
import numpy as np
import math
from hasty_settings import *
import hasty_accepted_analysis

NBINS = 20
NLEVELS = 20
e = 0
KSZ_CUTOFF = 0.5e5

A = hasty_accepted_analysis.AnalyseObs()

xx = []
xx2 = []
yy = []

for chain in A.chain:
	for model in chain:
		try:
			tmpxx = model['Atb']
			tmpxx2 = model['obsLSS']['H0']
			
			ksz = model['obsKSZ']
			tmpyy = A.interpolate_ksz(ksz, 0.3)
		
			if not np.isnan(tmpyy) and abs(tmpyy) < KSZ_CUTOFF:
				xx.append(tmpxx)
				xx2.append(tmpxx2)
				yy.append(tmpyy)
			else:
				"Invalid value for", tmpxx, tmpxx2, tmpyy
		except Exception, msg:
			#print "\t", msg
			e += 1
			pass

print "Likelihood plot calculated from", len(xx), "models."
print "There were", e, "models with errors."


# Calculate means
u_Atb = 0.0
u_H0 = 0.0
u_ksz3 = 0.0
for i in range(len(xx)):
	u_Atb += xx[i]
	u_H0 += xx2[i]
	u_ksz3 += yy[i]
n = float(len(xx))
u_Atb /= n
u_H0 /= n
u_ksz3 /= n

# Calculate variances
var_Atb = 0.0
var_H0 = 0.0
var_ksz3 = 0.0
for i in range(len(xx)):
	var_Atb += (u_Atb - xx[i])**2.0
	var_H0 += (u_H0 - xx2[i])**2.0
	var_ksz3 += (u_ksz3 - yy[i])**2.0
var_Atb /= n
var_H0 /= n
var_ksz3 /= n

# Output means and variances
print "\n----------------"
print "<Atb> =", u_Atb, "Var =", var_Atb, "s =", math.sqrt(var_Atb)
print "<H0>  =", u_H0, "Var =", var_H0, "s =", math.sqrt(var_H0)
print "<vp>  =", u_ksz3, "Var =", var_ksz3, "s =", math.sqrt(var_ksz3)
print "----------------\n"


# Make tB vs. kSZ histogram
P.subplot(221)
z, x, y = np.histogram2d(xx, yy, bins=NBINS)

def get_sig_levels(z):
	"""Get values corresponding to the different significance levels for a histo - argument is the histogrammed data"""
	
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	
	acc = 0.0
	i=-1
	j=0
	lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j]):
			print "Reached " + str(j+1) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]

slevels = get_sig_levels(z)
P.contourf(y[:-1], x[:-1], z, slevels)
#P.plot(yy, xx, 'w,')
P.grid(True)
P.ylabel("$A_{tB}$ [Myr]")
P.xlabel("kSZ(z=0.3) [km/s]")

# Make H0 vs. kSZ histogram
P.subplot(222)
#z, x, y = np.histogram2d(xx2, yy, bins=NBINS)
P.contourf(y[:-1], x[:-1], z, NLEVELS)
P.grid(True)
P.ylabel("$H_0$ [100 km/s/Mpc]")
P.xlabel("kSZ(z=0.3) [km/s]")


# Make tB histogram
P.subplot(223)
n, bins, patches = P.hist(xx, bins=50, facecolor='r')
xxx = np.arange(min(xx), max(xx), (max(xx) - min(xx))/100.)
P.plot(xxx, map(lambda f: max(n)*math.exp(-((f-u_Atb)**2.)*0.5/var_Atb), xxx), 'k-')
P.xlabel("$A_{tB}$ [Myr]")

# Make kSZ histogram
P.subplot(224)
P.hist(yy, bins=50)
P.xlabel("kSZ(z=0.3) [km/s]")

P.show()
