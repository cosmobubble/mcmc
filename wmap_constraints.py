#!/usr/bin/python
"""Use WMAP data to derive errors on desired params in a Monte Carlo way"""
# Most expressions from Clarkson + Regis 2010 arxiv:1007.3443

import math
import numpy as np
import numpy.random as nran
import pylab as P
import lcdm_parameters as L
import wmap_covariance_matrix as W
import shift_params as SP

nran.seed() # Seed random number generator
N = int(2e3) # Number of samples to generate

# Get parameters with WMAP 7 values, covariances
mean, cov = W.cov() # (omegam, omegal, H0, zeq, zstar)
for i in range(len(mean)):
	print "<" + W.variables[i] + "> =", round(mean[i], 4),
	print "+/-", round(math.sqrt(cov[i][i]), 4)
print "--------------"

# Generate array of random draws of these parameters
draws = []
for i in range(N):
	draws.append( nran.multivariate_normal(mean, cov) )

# For each draw, build a model and evaluate the shift parameters
S = []
dH = []
dA = []
Hlss = []
print "Evaluating shift parameters"
for i in range(len(draws)):
	if i%50 == 0:
		print str(round(100.0*float(i)/float(N),1)) + "%"
	om = draws[i][0]
	ol = draws[i][1]
	H0 = draws[i][2]
	#zeq = draws[i][3]
	zeq = 3238.61
	zstar = draws[i][4]
	mod = L.Model(H0, om, ol, zeq) # Make a model
	# Get observables
	obs_dA = mod.dA(zstar)*(1.+zstar)
	obs_Hlss = mod.H(zstar)
	S.append( SP.Shift(obs_dA, obs_Hlss, zstar, zeq) ) # dA-dependent shift param.
	dH.append( SP.dH(H0, obs_Hlss, zstar, zeq) ) # H0-dependent shift param.
	dA.append( obs_dA ) # Calculate dA(zstar)
	Hlss.append( obs_Hlss ) # Calculate Hlss(zstar)
print "Finished.\n"

# Calculate means/std.dev. etc.
print "<S> =", round(np.mean(S), 4), "+/-", round(np.std(S), 4)
print "<dH> =", round(np.mean(dH), 4), "+/-", round(np.std(dH), 4)
print "<dA> =", round(np.mean(dA), 4), "+/-", round(np.std(dA), 4)
print "<Hlss> =", round(np.mean(Hlss), 4), "+/-", round(np.std(Hlss), 4)
print " "

print "COVARIANCE (S,dH)"
dat = []
dat.append(S)
dat.append(dH)
print np.cov(dat)

"""
# Draw binned histogram of shift parameters
P.subplot(221)
P.hist(S, bins=50, fc='r', normed=True)
P.xlabel("S")

P.subplot(222)
z, x, y = np.histogram2d(S, dH, bins=20)
P.contourf(y[:-1], x[:-1], z)

P.subplot(223)
P.hist(W.get_data(0), bins=20)
P.xlabel('OmegaM')

P.subplot(224)
P.hist(W.get_data(3), bins=20)
P.xlabel('zeq')

P.show()
"""
